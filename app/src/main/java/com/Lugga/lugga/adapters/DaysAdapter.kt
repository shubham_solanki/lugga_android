package com.Lugga.lugga.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.Lugga.lugga.R
import com.Lugga.lugga.model.DaysList
import kotlinx.android.synthetic.main.item_days.view.*


class DaysAdapter(val context: Context, var list: ArrayList<DaysList>?) : RecyclerView.Adapter<DaysAdapter.Holder>() {
    var selectedDataList: ArrayList<DaysList>?=null

    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(LayoutInflater.from(parent.context).inflate(R.layout.item_days, null))
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {

        val model = list?.get(position)

        holder.itemView.tvDay.text = model?.day


        holder.itemView.setOnClickListener {
            if (!model!!.isSelected) {
                model.isSelected = true
                holder.itemView.tvCheck.background = ContextCompat.getDrawable(context, R.drawable.back_selected_day)
            } else {
                model.isSelected = false
                holder.itemView.tvCheck.background = ContextCompat.getDrawable(context, R.drawable.back_unselected_day)
            }

        }



    }

    fun getSelected(): ArrayList<DaysList>? {
        selectedDataList= ArrayList()
        for (i in 0 until list!!.size) {
            if (list!![i].isSelected) {
                selectedDataList!!.add(list!!.get(i))
            }
        }
        return selectedDataList
    }

    fun clear() {
        selectedDataList!!.clear()
    }
}