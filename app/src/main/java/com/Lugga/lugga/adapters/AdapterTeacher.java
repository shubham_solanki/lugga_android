package com.Lugga.lugga.adapters;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import de.hdodenhof.circleimageview.CircleImageView;

import com.Lugga.lugga.model.TimerModel;
import com.Lugga.lugga.utils.ApiClient;
import com.bumptech.glide.Glide;
import com.Lugga.lugga.R;
import com.Lugga.lugga.model.teacherhomepage.TeacherHomePageBodyItem;
import com.Lugga.lugga.sharedpreferences.UsersharedPrefernce;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class AdapterTeacher extends RecyclerView.Adapter<AdapterTeacher.ViewHolder> {
    private Context context;

    UsersharedPrefernce usersharedPrefernce;
    LinearLayout starlayout;
    onpayclick onpayclick;


    ArrayList<TeacherHomePageBodyItem> teacherHomePageBodyItemArrayList;
    private Date ConvertedTimeWithZone;
    private String currentdateTimeApi;
    private Date datee;

    private static final String DATE_FORMAT = "dd-MM-yyyy HH:mm:ss a z";
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
    private String year1, month, dayy;
    private String serverTime;
    private Date s;
    private DateFormat fr;
    private int millis;
    private CountDownTimer countDownTimer;
    private Date startSessionTim;
    private String finalSecond;
    private String finalMinute;
    private String myhour;
    private String mySecond, myMinute;
    private TimerModel timerModel;
    private Dialog mDialog;


    public AdapterTeacher(Context context, ArrayList<TeacherHomePageBodyItem> teacherHomePageBodyItemArrayList) {
        this.context = context;
        this.teacherHomePageBodyItemArrayList = teacherHomePageBodyItemArrayList;

    }

    public void setonPayClickListener(AdapterTeacher.onpayclick onpayeclick) {
        this.onpayclick = onpayeclick;
    }


    @Override
    public AdapterTeacher.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        usersharedPrefernce = UsersharedPrefernce.getInstance();

        View view = LayoutInflater.from(context).inflate(R.layout.item_teacher_home, parent, false);

        return new AdapterTeacher.ViewHolder(view);
    }

    @TargetApi(Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(final AdapterTeacher.ViewHolder holder, final int position) {
        holder.starLayout.setVisibility(View.GONE);

        if (teacherHomePageBodyItemArrayList.get(position).getLevel().equals("1")) {
            holder.tvGoals.setText("Beginner");

        } else if (teacherHomePageBodyItemArrayList.get(position).getLevel().equals("2")) {
            holder.tvGoals.setText("Intermediate");

        } else {
            holder.tvGoals.setText("Advanced");

        }
        if (teacherHomePageBodyItemArrayList.get(position).getProfile_image() != null) {
            Glide.with(context).load(ApiClient.IMAGE_URL + teacherHomePageBodyItemArrayList.get(position).getProfile_image()).
                    centerCrop().into(holder.profile);
        } else {
            holder.profile.setImageResource(R.drawable.person_girl);
        }
        holder.personName.setText(teacherHomePageBodyItemArrayList.get(position).getName());
        //holder.tvRating.setText(teacherHomePageBodyItemArrayList.get(position).getRating());
        //   holder.paymentamount.setText("" + teacherHomePageBodyItemArrayList.get(position).getPaymentAmount());
        holder.time.setText(teacherHomePageBodyItemArrayList.get(position).getSessionTime() + " min");


        if (teacherHomePageBodyItemArrayList.get(position).getStatus() == 5 ||
                teacherHomePageBodyItemArrayList.get(position).getStatus() == 6) {
            holder.llCurruntNotification.setVisibility(View.GONE);

            holder.paymentamount.setText("$" + teacherHomePageBodyItemArrayList.get(position).getPaymentAmount());
        } else {
            holder.llCurruntNotification.setVisibility(View.GONE);

            switch (teacherHomePageBodyItemArrayList.get(position).getSessionTime()) {
                case "30":
                    holder.paymentamount.setText("$" + teacherHomePageBodyItemArrayList.get(position).getAmount());

                    break;
                case "60":
                    holder.paymentamount.setText("$" + teacherHomePageBodyItemArrayList.get(position).getAmount() * 2);

                    break;

                case "90":
                    holder.paymentamount.setText("$" + teacherHomePageBodyItemArrayList.get(position).getAmount() * 3);
                    break;

                case "120":
                    holder.paymentamount.setText("$" + teacherHomePageBodyItemArrayList.get(position).getAmount() * 4);
                    break;
            }
        }





        /*if (teacherHomePageBodyItemArrayList.get(position).getRating().equals("0")) {
            holder.star.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.star_grey));
        } else {
            holder.star.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.star_yellow));
        }*/

        //**************************************getting date and time****************************************//

        String createdAt = teacherHomePageBodyItemArrayList.get(position).getBookingdate();
        //  System.out.println("ddcdcdc" + createdAt);
        String parts[] = createdAt.split("T");
//        System.out.println("ffks   " + parts[0] + "    " + parts[1]);
        String date = parts[0];
        String day[] = date.split("-");
        //  System.out.println("nckskdsds" + day[0] + "   " + day[1] + "   " + day[2]);
        holder.date.setText(day[2] + "/" + day[1] + "/" + day[0]);

//        String sessionstarttime = teacherHomePageBodyItemArrayList.get(position).getStartSession();
//        String timeparts[] = sessionstarttime.split(":");
//        int time1 = Integer.parseInt(timeparts[0]);
//        int newtime1 = 0;
//        if (time1 > 12) {
//            newtime1 = time1 - 12;
//        } else {
//            newtime1 = time1;
//        }
//        holder.endTime.setText(newtime1 + ":" + timeparts[1]);
//        int minute = Integer.parseInt(timeparts[1]);
//        int seconds = Integer.parseInt(timeparts[2]);


        if (teacherHomePageBodyItemArrayList.get(position).getStatus() == 1) {
            holder.llCurruntNotification.setVisibility(View.GONE);

            holder.textStatus.setText("Rejected");
            String apiTimezone = teacherHomePageBodyItemArrayList.get(position).getTimezone();
            String apiBookingDate = teacherHomePageBodyItemArrayList.get(position).getBookingdate();
            String subStrBookingDate = apiBookingDate.substring(0, 10);


            Calendar cal = Calendar.getInstance();
            long milliDiff = cal.get(Calendar.ZONE_OFFSET);           // Got local offset, now loop through available timezone id(s).
            String[] ids = TimeZone.getAvailableIDs();
            String name = null;
            for (String id : ids) {
                TimeZone tz = TimeZone.getTimeZone(id);
                if (tz.getRawOffset() == milliDiff) {
                    // Found a match.
                    name = id;
                    break;
                }
            }
            System.out.println("ZoneId " + name);
            ZoneId fromTimeZone = ZoneId.of(apiTimezone);    //Source timezone

            TimeZone timeZone = TimeZone.getDefault();

            ZoneId toTimeZone = ZoneId.of(timeZone.getID());  //Target timezone

            //     ZoneId toTimeZone = ZoneId.of(name);  //Target timezone


            String newDate = subStrBookingDate.replace("-", "");
            String finalYear = newDate.substring(0, 4);
            String finalMonth = newDate.substring(4, 6);
            String finalDay = newDate.substring(6, 8);

            String filename = teacherHomePageBodyItemArrayList.get(position).getStartSession();     // full file name
            String[] colon = filename.split("\\:"); // String array, each element is text between dots

            String firststr = colon[0];
            String secondstr = colon[1];
            String thirdstr = colon[2];
            if (firststr.length() < 2) {
                myhour = "0" + firststr;
            } else {
                myhour = firststr;
            }
            if (secondstr.length() < 2) {
                myMinute = "0" + secondstr;
            } else {
                myMinute = secondstr;
            }

            if (thirdstr.length() < 2) {
                mySecond = "0" + thirdstr;
            } else {
                mySecond = thirdstr;
            }
            Log.e("yesssssssssss", myhour);
            Log.e("yesssssssssss", myMinute);
            Log.e("yesssssssssss", mySecond);


            ZonedDateTime currentISTime =
                    ZonedDateTime.of(Integer.parseInt(finalYear), Integer.parseInt(finalMonth),
                            Integer.parseInt(finalDay), Integer.parseInt(myhour), Integer.parseInt(myMinute),
                            Integer.parseInt(mySecond), 1234, fromTimeZone);


            ZonedDateTime currentETime = currentISTime.withZoneSameInstant(toTimeZone);

            //Format date time - optional
            System.out.println("from " + formatter.format(currentISTime));
            System.out.println("to " + formatter.format(currentETime));


            String dtStart = formatter.format(currentETime).substring(10, 21);
            holder.endTime.setText(dtStart);

            // System.out.println("timeinmiikis" + getTimeInMillis(newtime1, minute, seconds) + "      " + System.currentTimeMillis());
        } else if (teacherHomePageBodyItemArrayList.get(position).getStatus() == 2) {

            holder.llCurruntNotification.setVisibility(View.GONE);

            holder.textStatus.setText("Accepted");
            String apiTimezone = teacherHomePageBodyItemArrayList.get(position).getTimezone();
            String apiBookingDate = teacherHomePageBodyItemArrayList.get(position).getBookingdate();
            String subStrBookingDate = apiBookingDate.substring(0, 10);


            Calendar cal = Calendar.getInstance();
            long milliDiff = cal.get(Calendar.ZONE_OFFSET);           // Got local offset, now loop through available timezone id(s).
            String[] ids = TimeZone.getAvailableIDs();
            String name = null;
            for (String id : ids) {
                TimeZone tz = TimeZone.getTimeZone(id);
                if (tz.getRawOffset() == milliDiff) {
                    // Found a match.
                    name = id;
                    break;
                }
            }
            System.out.println("ZoneId " + name);
            ZoneId fromTimeZone = ZoneId.of(apiTimezone);    //Source timezone

            TimeZone timeZone = TimeZone.getDefault();

            ZoneId toTimeZone = ZoneId.of(timeZone.getID());  //Target timezone

            //   ZoneId toTimeZone = ZoneId.of(name);  //Target timezone


            String newDate = subStrBookingDate.replace("-", "");
            String finalYear = newDate.substring(0, 4);
            String finalMonth = newDate.substring(4, 6);
            String finalDay = newDate.substring(6, 8);

            String filename = teacherHomePageBodyItemArrayList.get(position).getStartSession();     // full file name
            String[] colon = filename.split("\\:"); // String array, each element is text between dots

            String firststr = colon[0];
            String secondstr = colon[1];
            String thirdstr = colon[2];
            if (firststr.length() < 2) {
                myhour = "0" + firststr;
            } else {
                myhour = firststr;
            }
            if (secondstr.length() < 2) {
                myMinute = "0" + secondstr;
            } else {
                myMinute = secondstr;
            }

            if (thirdstr.length() < 2) {
                mySecond = "0" + thirdstr;
            } else {
                mySecond = thirdstr;
            }
            Log.e("yesssssssssss", myhour);
            Log.e("yesssssssssss", myMinute);
            Log.e("yesssssssssss", mySecond);


            ZonedDateTime currentISTime =
                    ZonedDateTime.of(Integer.parseInt(finalYear), Integer.parseInt(finalMonth),
                            Integer.parseInt(finalDay), Integer.parseInt(myhour), Integer.parseInt(myMinute),
                            Integer.parseInt(mySecond), 1234, fromTimeZone);


            ZonedDateTime currentETime = currentISTime.withZoneSameInstant(toTimeZone);

            //Format date time - optional
            System.out.println("from " + formatter.format(currentISTime));
            System.out.println("to " + formatter.format(currentETime));


            String dtStart = formatter.format(currentETime).substring(10, 21);
            holder.endTime.setText(dtStart);

        } else if (teacherHomePageBodyItemArrayList.get(position).getStatus() == 3) {
            holder.llCurruntNotification.setVisibility(View.GONE);

            holder.textStatus.setText("Upcoming");
            String apiTimezone = teacherHomePageBodyItemArrayList.get(position).getTimezone();
            String apiBookingDate = teacherHomePageBodyItemArrayList.get(position).getBookingdate();
            String subStrBookingDate = apiBookingDate.substring(0, 10);


            Calendar cal = Calendar.getInstance();
            long milliDiff = cal.get(Calendar.ZONE_OFFSET);           // Got local offset, now loop through available timezone id(s).
            String[] ids = TimeZone.getAvailableIDs();
            String name = null;
            for (String id : ids) {
                TimeZone tz = TimeZone.getTimeZone(id);
                if (tz.getRawOffset() == milliDiff) {
                    // Found a match.
                    name = id;
                    break;
                }
            }
            System.out.println("ZoneId " + name);
            ZoneId fromTimeZone = ZoneId.of(apiTimezone);    //Source timezone

            TimeZone timeZone = TimeZone.getDefault();

            ZoneId toTimeZone = ZoneId.of(timeZone.getID());  //Target timezone

            //   ZoneId toTimeZone = ZoneId.of(name);  //Target timezone


            String newDate = subStrBookingDate.replace("-", "");
            String finalYear = newDate.substring(0, 4);
            String finalMonth = newDate.substring(4, 6);
            String finalDay = newDate.substring(6, 8);

            String filename = teacherHomePageBodyItemArrayList.get(position).getStartSession();     // full file name
            String[] colon = filename.split("\\:"); // String array, each element is text between dots

            String firststr = colon[0];
            String secondstr = colon[1];
            String thirdstr = colon[2];
            if (firststr.length() < 2) {
                myhour = "0" + firststr;
            } else {
                myhour = firststr;
            }
            if (secondstr.length() < 2) {
                myMinute = "0" + secondstr;
            } else {
                myMinute = secondstr;
            }

            if (thirdstr.length() < 2) {
                mySecond = "0" + thirdstr;
            } else {
                mySecond = thirdstr;
            }
            Log.e("yesssssssssss", myhour);
            Log.e("yesssssssssss", myMinute);
            Log.e("yesssssssssss", mySecond);


            ZonedDateTime currentISTime =
                    ZonedDateTime.of(Integer.parseInt(finalYear), Integer.parseInt(finalMonth),
                            Integer.parseInt(finalDay), Integer.parseInt(myhour), Integer.parseInt(myMinute),
                            Integer.parseInt(mySecond), 1234, fromTimeZone);


            ZonedDateTime currentETime = currentISTime.withZoneSameInstant(toTimeZone);

            //Format date time - optional
            System.out.println("from " + formatter.format(currentISTime));
            System.out.println("to " + formatter.format(currentETime));


            String dtStart = formatter.format(currentETime).substring(10, 21);
            holder.endTime.setText(dtStart);

            holder.textStatus.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
        } else if (teacherHomePageBodyItemArrayList.get(position).getStatus() == 4) {
            holder.llCurruntNotification.setVisibility(View.GONE);

            holder.textStatus.setText("Waiting for learner to pay (Learner has 30 min to pay)");
            String apiTimezone = teacherHomePageBodyItemArrayList.get(position).getTimezone();
            String apiBookingDate = teacherHomePageBodyItemArrayList.get(position).getBookingdate();
            String subStrBookingDate = apiBookingDate.substring(0, 10);


            Calendar cal = Calendar.getInstance();
            long milliDiff = cal.get(Calendar.ZONE_OFFSET);           // Got local offset, now loop through available timezone id(s).
            String[] ids = TimeZone.getAvailableIDs();
            String name = null;

            for (String id : ids) {
                TimeZone tz = TimeZone.getTimeZone(id);
                if (tz.getRawOffset() == milliDiff) {
                    // Found a match.
                    name = id;
                    break;
                }
            }

            System.out.println("ZoneId " + name);
            ZoneId fromTimeZone = ZoneId.of(apiTimezone);    //Source timezone
            TimeZone timeZone = TimeZone.getDefault();

            ZoneId toTimeZone = ZoneId.of(timeZone.getID());  //Target timezone


            String newDate = subStrBookingDate.replace("-", "");
            String finalYear = newDate.substring(0, 4);
            String finalMonth = newDate.substring(4, 6);
            String finalDay = newDate.substring(6, 8);

            String filename = teacherHomePageBodyItemArrayList.get(position).getStartSession();     // full file name
            String[] colon = filename.split("\\:"); // String array, each element is text between dots

            String firststr = colon[0];
            String secondstr = colon[1];
            String thirdstr = colon[2];
            if (firststr.length() < 2) {
                myhour = "0" + firststr;
            } else {
                myhour = firststr;
            }
            if (secondstr.length() < 2) {
                myMinute = "0" + secondstr;
            } else {
                myMinute = secondstr;
            }

            if (thirdstr.length() < 2) {
                mySecond = "0" + thirdstr;
            } else {
                mySecond = thirdstr;
            }
            Log.e("yesssssssssss", myhour);
            Log.e("yesssssssssss", myMinute);
            Log.e("yesssssssssss", mySecond);


            ZonedDateTime currentISTime =
                    ZonedDateTime.of(Integer.parseInt(finalYear), Integer.parseInt(finalMonth),
                            Integer.parseInt(finalDay), Integer.parseInt(myhour), Integer.parseInt(myMinute),
                            Integer.parseInt(mySecond), 1234, fromTimeZone);


            ZonedDateTime currentETime = currentISTime.withZoneSameInstant(toTimeZone);

            //Format date time - optional
            System.out.println("from " + formatter.format(currentISTime));
            System.out.println("to " + formatter.format(currentETime));


            String dtStart = formatter.format(currentETime).substring(10, 20);
            holder.endTime.setText(dtStart);

        } else if (teacherHomePageBodyItemArrayList.get(position).getStatus() == 5) {
            holder.llCurruntNotification.setVisibility(View.GONE);

            String apiTimezone = teacherHomePageBodyItemArrayList.get(position).getTimezone();
            String apiBookingDate = teacherHomePageBodyItemArrayList.get(position).getBookingdate();
            String subStrBookingDate = apiBookingDate.substring(0, 10);


            Calendar cal = Calendar.getInstance();
            long milliDiff = cal.get(Calendar.ZONE_OFFSET);           // Got local offset, now loop through available timezone id(s).
            String[] ids = TimeZone.getAvailableIDs();
            String name = null;
            for (String id : ids) {
                TimeZone tz = TimeZone.getTimeZone(id);
                if (tz.getRawOffset() == milliDiff) {
                    // Found a match.
                    name = id;
                    break;
                }
            }
            //  System.out.println("ZoneId " + name);
            ZoneId fromTimeZone = ZoneId.of(apiTimezone);    //Source timezone

            TimeZone timeZone = TimeZone.getDefault();

            ZoneId toTimeZone = ZoneId.of(timeZone.getID());  //Target timezone

            //   ZoneId toTimeZone = ZoneId.of(name);  //Target timezone


            String newDate = subStrBookingDate.replace("-", "");
            String finalYear = newDate.substring(0, 4);
            String finalMonth = newDate.substring(4, 6);
            String finalDay = newDate.substring(6, 8);

            String filename = teacherHomePageBodyItemArrayList.get(position).getStartSession();     // full file name
            String[] colon = filename.split("\\:"); // String array, each element is text between dots

            String firststr = colon[0];
            String secondstr = colon[1];
            String thirdstr = colon[2];
            if (firststr.length() < 2) {
                myhour = "0" + firststr;
            } else {
                myhour = firststr;
            }
            if (secondstr.length() < 2) {
                myMinute = "0" + secondstr;
            } else {
                myMinute = secondstr;
            }

            if (thirdstr.length() < 2) {
                mySecond = "0" + thirdstr;
            } else {
                mySecond = thirdstr;
            }
            Log.e("yesssssssssss", myhour);
            Log.e("yesssssssssss", myMinute);
            Log.e("yesssssssssss", mySecond);


            ZonedDateTime currentISTime =
                    ZonedDateTime.of(Integer.parseInt(finalYear), Integer.parseInt(finalMonth),
                            Integer.parseInt(finalDay), Integer.parseInt(myhour), Integer.parseInt(myMinute),
                            Integer.parseInt(mySecond), 1234, fromTimeZone);


            ZonedDateTime currentETime = currentISTime.withZoneSameInstant(toTimeZone);

            //Format date time - optional
            System.out.println("from " + formatter.format(currentISTime));
            System.out.println("to " + formatter.format(currentETime));


            String dtStart = formatter.format(currentETime).substring(10, 20);
            holder.endTime.setText(dtStart);


            //-------compare time----------


            String convert = formatter.format(currentETime).substring(0, 19);
            String reverse = convert.substring(0, 10);


            final String OLD_FORMAT = "dd-MM-yyyy";
            final String NEW_FORMAT = "yyyy-MM-dd";
            String oldDateString = reverse;
            String newDateString;
            SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
            Date d1 = null;
            try {
                d1 = sdf.parse(oldDateString);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            sdf.applyPattern(NEW_FORMAT);
            newDateString = sdf.format(d1);

            // Log.e("serverNewDate",newDateString);


            String endTimeStr = newDateString + "" + formatter.format(currentETime).substring(10, 19);        /// finalDateTime for comparing with server time


            String sessionStartTime = endTimeStr;

            String startSessionTime = newDateString + " " + formatter.format(currentETime).substring(10, 19);

            /// adding session time with start session time===================

            String myTime = sessionStartTime;
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date d = null;
            try {
                d = df.parse(myTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Calendar calender = Calendar.getInstance();
            calender.setTime(d);
            calender.add(Calendar.MINUTE, Integer.parseInt(teacherHomePageBodyItemArrayList.get(0).getSessionTime()));
            String newTimeAferAdding = df.format(calender.getTime());

            String sessionCloseTime = newTimeAferAdding;

            Log.e("sessionCloseTime", sessionCloseTime);
            String SessionEndTimeFormat = sessionCloseTime;

            ////////////convert currentDateTimeServer to local time--------------------
            // 2020-05-06 00:11:39


            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            formatter.setTimeZone(TimeZone.getTimeZone("America/Los_Angeles")); // Or whatever IST is supposed to be
            try {
                s = formatter.parse(serverTime);
                fr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                fr.setTimeZone(TimeZone.getTimeZone(timeZone.getID()));
                Log.e("timeNeewhai", fr.format(s));

            } catch (ParseException e) {
                Log.e("error", e.toString());
            }


            String startTimeStr = fr.format(s);

            Date serverCurrentDateTime = null;
            Date endSessionTime = null;
            try {
                SimpleDateFormat sdf12 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
                serverCurrentDateTime = sdf12.parse(startTimeStr);
                endSessionTime = sdf12.parse(SessionEndTimeFormat);


                Log.e("EndsessionTime---------", String.valueOf(endSessionTime));
                Log.e("ServerCurrentDateTime-----", String.valueOf(serverCurrentDateTime));


            } catch (Exception e) {
                e.printStackTrace();
            }


            /////comparing start session time


            startSessionTim = null;
            try {
                SimpleDateFormat sdf12 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                serverCurrentDateTime = sdf12.parse(startTimeStr);
                startSessionTim = sdf12.parse(startSessionTime);

            } catch (Exception e) {
                e.printStackTrace();
            }


            //--------Compare time -------
            if (teacherHomePageBodyItemArrayList.get(position).getBookingType() == 1) {


                if (serverCurrentDateTime.compareTo(startSessionTim) < 0) {        /////bada hai startSessionTim   -- booking not started/ongoing booking
                    holder.textStatus.setText("Scheduled");
                    holder.textCounter.setVisibility(View.INVISIBLE);

                    holder.llDateTime.setVisibility(View.VISIBLE);


                    Log.e("app", "serverCurrentDateTime is before endSessionTime");


                } else if (serverCurrentDateTime.compareTo(endSessionTime) < 0) {        /////bada hai endSessionTime   -- booking not started/ongoing booking
                    holder.textStatus.setText("Ongoing");
                    holder.textStatus.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));

                    holder.llDateTime.setVisibility(View.INVISIBLE);
                    holder.textCounter.setVisibility(View.VISIBLE);
                    long mills = endSessionTime.getTime() - serverCurrentDateTime.getTime();

                    long hours = mills / (1000 * 60 * 60);
                    long mins = (mills / (1000 * 60)) % 60;

                    String diff = hours + ":" + mins;
                    timerModel = new TimerModel();
                    Log.e("difffff", String.valueOf(mills));

                    countDownTimer = new CountDownTimer(mills, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {
                            holder.textCounter.setText("" + String.format("%d : %d ",
                                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));

                            timerModel.setMin(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished));
                            timerModel.setSec(TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)));
                        }

                        @Override
                        public void onFinish() {
                            countDownTimer.cancel();
                            onpayclick.hitCompletedBookingApi(position, "6", teacherHomePageBodyItemArrayList.get(position).getBooking_id());

                        }
                    };
                    countDownTimer.start();

                    Log.e("app", "serverCurrentDateTime is before endSessionTime");


                } else {
                    onpayclick.hitCompletedBookingApi(position, "6", teacherHomePageBodyItemArrayList.get(position).getBooking_id());
                }
            } else {

                holder.textStatus.setText("Ongoing");
                holder.textStatus.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));


                holder.llDateTime.setVisibility(View.INVISIBLE);
                holder.textCounter.setVisibility(View.VISIBLE);
                //--------Compare endsession time -------

                if (serverCurrentDateTime.compareTo(endSessionTime) > 0) {       /////chota hai endSessionTime

                    Log.e("app", "serverCurrentDateTime is after endSessionTime");
                    onpayclick.hitCompletedBookingApi(position, "6", teacherHomePageBodyItemArrayList.get(position).getBooking_id());

                } else if (serverCurrentDateTime.compareTo(endSessionTime) < 0) {        /////bada hai endSessionTime   -- booking not started/ongoing booking


                    long mills = endSessionTime.getTime() - serverCurrentDateTime.getTime();
                    long hours = mills / (1000 * 60 * 60);
                    long mins = (mills / (1000 * 60)) % 60;
                    timerModel = new TimerModel();
                    String diff = hours + ":" + mins;

                    Log.e("difffff", String.valueOf(mills));

                    countDownTimer = new CountDownTimer(mills, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {
                            holder.textCounter.setText("" + String.format("%d : %d ",
                                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                            timerModel.setMin(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished));
                            timerModel.setSec(TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)));
                        }

                        @Override
                        public void onFinish() {
                            countDownTimer.cancel();
                            onpayclick.hitCompletedBookingApi(position, "6", teacherHomePageBodyItemArrayList.get(position).getBooking_id());
                        }
                    };
                    countDownTimer.start();

                    Log.e("app", "serverCurrentDateTime is before endSessionTime");


                } else if (serverCurrentDateTime.compareTo(endSessionTime) == 0) {

                    Log.e("app", "serverCurrentDateTime is equal to endSessionTime");
                }


            }


        } else if (teacherHomePageBodyItemArrayList.get(position).getStatus() == 6) {
            holder.textStatus.setText("Completed");
            String apiTimezone = teacherHomePageBodyItemArrayList.get(position).getTimezone();
            String apiBookingDate = teacherHomePageBodyItemArrayList.get(position).getBookingdate();
            String subStrBookingDate = apiBookingDate.substring(0, 10);


            Calendar cal = Calendar.getInstance();
            long milliDiff = cal.get(Calendar.ZONE_OFFSET);           // Got local offset, now loop through available timezone id(s).
            String[] ids = TimeZone.getAvailableIDs();
            String name = null;
            for (String id : ids) {
                TimeZone tz = TimeZone.getTimeZone(id);
                if (tz.getRawOffset() == milliDiff) {
                    // Found a match.
                    name = id;
                    break;
                }
            }
            System.out.println("ZoneId " + name);
            ZoneId fromTimeZone = ZoneId.of(apiTimezone);    //Source timezone

            TimeZone timeZone = TimeZone.getDefault();
            ZoneId toTimeZone = ZoneId.of(timeZone.getID());  //Target timezone


            //  ZoneId toTimeZone = ZoneId.of(name);  //Target timezone


            String newDate = subStrBookingDate.replace("-", "");
            String finalYear = newDate.substring(0, 4);
            String finalMonth = newDate.substring(4, 6);
            String finalDay = newDate.substring(6, 8);


            String filename = teacherHomePageBodyItemArrayList.get(position).getStartSession();     // full file name
            String[] colon = filename.split("\\:"); // String array, each element is text between dots

            String firststr = colon[0];
            String secondstr = colon[1];
            String thirdstr = colon[2];
            if (firststr.length() < 2) {
                myhour = "0" + firststr;
            } else {
                myhour = firststr;
            }
            if (secondstr.length() < 2) {
                myMinute = "0" + secondstr;
            } else {
                myMinute = secondstr;
            }

            if (thirdstr.length() < 2) {
                mySecond = "0" + thirdstr;
            } else {
                mySecond = thirdstr;
            }

            Log.e("yesssssssssss", myhour);
            Log.e("yesssssssssss", myMinute);
            Log.e("yesssssssssss", mySecond);


            ZonedDateTime currentISTime =
                    ZonedDateTime.of(Integer.parseInt(finalYear), Integer.parseInt(finalMonth),
                            Integer.parseInt(finalDay), Integer.parseInt(myhour), Integer.parseInt(myMinute),
                            Integer.parseInt(mySecond), 1234, fromTimeZone);

            ZonedDateTime currentETime = currentISTime.withZoneSameInstant(toTimeZone);

            //Format date time - optional
            System.out.println("from " + formatter.format(currentISTime));
            System.out.println("to " + formatter.format(currentETime));


            String dtStart = formatter.format(currentETime).substring(10, 20);
            holder.endTime.setText(dtStart);

        } else if (teacherHomePageBodyItemArrayList.get(position).getStatus() == 7) {
            holder.textStatus.setText("Expired");
            holder.llCurruntNotification.setVisibility(View.GONE);

            String apiTimezone = teacherHomePageBodyItemArrayList.get(position).getTimezone();
            String apiBookingDate = teacherHomePageBodyItemArrayList.get(position).getBookingdate();
            String subStrBookingDate = apiBookingDate.substring(0, 10);


            Calendar cal = Calendar.getInstance();
            long milliDiff = cal.get(Calendar.ZONE_OFFSET);           // Got local offset, now loop through available timezone id(s).
            String[] ids = TimeZone.getAvailableIDs();
            String name = null;
            for (String id : ids) {
                TimeZone tz = TimeZone.getTimeZone(id);
                if (tz.getRawOffset() == milliDiff) {
                    // Found a match.
                    name = id;
                    break;
                }
            }
            System.out.println("ZoneId " + name);
            ZoneId fromTimeZone = ZoneId.of(apiTimezone);    //Source timezone

            TimeZone timeZone = TimeZone.getDefault();
            ZoneId toTimeZone = ZoneId.of(timeZone.getID());  //Target timezone

            //    ZoneId toTimeZone = ZoneId.of(name);  //Target timezone


            String newDate = subStrBookingDate.replace("-", "");
            String finalYear = newDate.substring(0, 4);
            String finalMonth = newDate.substring(4, 6);
            String finalDay = newDate.substring(6, 8);

            String newTime = teacherHomePageBodyItemArrayList.get(position).getStartSession().replace(":", "");
            String filename = teacherHomePageBodyItemArrayList.get(position).getStartSession();     // full file name
            String[] colon = filename.split("\\:"); // String array, each element is text between dots

            String firststr = colon[0];
            String secondstr = colon[1];
            String thirdstr = colon[2];
            if (firststr.length() < 2) {
                myhour = "0" + firststr;
            } else {
                myhour = firststr;
            }
            if (secondstr.length() < 2) {
                myMinute = "0" + secondstr;
            } else {
                myMinute = secondstr;
            }

            if (thirdstr.length() < 2) {
                mySecond = "0" + thirdstr;
            } else {
                mySecond = thirdstr;
            }
            Log.e("yesssssssssss", myhour);
            Log.e("yesssssssssss", myMinute);
            Log.e("yesssssssssss", mySecond);


            ZonedDateTime currentISTime =
                    ZonedDateTime.of(Integer.parseInt(finalYear), Integer.parseInt(finalMonth),
                            Integer.parseInt(finalDay), Integer.parseInt(myhour), Integer.parseInt(myMinute),
                            Integer.parseInt(mySecond), 1234, fromTimeZone);


            ZonedDateTime currentETime = currentISTime.withZoneSameInstant(toTimeZone);

            //Format date time - optional
            System.out.println("from " + formatter.format(currentISTime));
            System.out.println("to " + formatter.format(currentETime));


            String dtStart = formatter.format(currentETime).substring(10, 20);
            holder.endTime.setText(dtStart);


        } else if (teacherHomePageBodyItemArrayList.get(position).getStatus() == 0) {
            holder.textStatus.setText("Waiting");

            holder.llCurruntNotification.setVisibility(View.VISIBLE);

            holder.tvAccept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    onpayclick.onAcceptRequest(position, "4", teacherHomePageBodyItemArrayList.get(position).getBooking_id(),
                            teacherHomePageBodyItemArrayList.get(position).getLearnerId());
                }
            });


            holder.tvReject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    onpayclick.onRejectRequest(position, "1", teacherHomePageBodyItemArrayList.get(position).getBooking_id(),
                            teacherHomePageBodyItemArrayList.get(position).getLearnerId());


                }
            });


            String apiTimezone = teacherHomePageBodyItemArrayList.get(position).getTimezone();
            String apiBookingDate = teacherHomePageBodyItemArrayList.get(position).getBookingdate();
            String subStrBookingDate = apiBookingDate.substring(0, 10);


            Calendar cal = Calendar.getInstance();
            long milliDiff = cal.get(Calendar.ZONE_OFFSET);           // Got local offset, now loop through available timezone id(s).
            String[] ids = TimeZone.getAvailableIDs();
            String name = null;
            for (String id : ids) {
                TimeZone tz = TimeZone.getTimeZone(id);
                if (tz.getRawOffset() == milliDiff) {
                    // Found a match.
                    name = id;
                    break;
                }
            }
            System.out.println("ZoneId " + name);
            ZoneId fromTimeZone = ZoneId.of(apiTimezone);    //Source timezone


            TimeZone timeZone = TimeZone.getDefault();

            ZoneId toTimeZone = ZoneId.of(timeZone.getID());  //Target timezone

            //   ZoneId toTimeZone = ZoneId.of(name);  //Target timezone


            String newDate = subStrBookingDate.replace("-", "");
            String finalYear = newDate.substring(0, 4);
            String finalMonth = newDate.substring(4, 6);
            String finalDay = newDate.substring(6, 8);

            String newTime = teacherHomePageBodyItemArrayList.get(position).getStartSession().replace(":", "");
            String filename = teacherHomePageBodyItemArrayList.get(position).getStartSession();     // full file name
            String[] colon = filename.split("\\:"); // String array, each element is text between dots

            String firststr = colon[0];
            String secondstr = colon[1];
            String thirdstr = colon[2];
            if (firststr.length() < 2) {
                myhour = "0" + firststr;
            } else {
                myhour = firststr;
            }
            if (secondstr.length() < 2) {
                myMinute = "0" + secondstr;
            } else {
                myMinute = secondstr;
            }

            if (thirdstr.length() < 2) {
                mySecond = "0" + thirdstr;
            } else {
                mySecond = thirdstr;
            }
            Log.e("yesssssssssss", myhour);
            Log.e("yesssssssssss", myMinute);
            Log.e("yesssssssssss", mySecond);


            ZonedDateTime currentISTime =
                    ZonedDateTime.of(Integer.parseInt(finalYear), Integer.parseInt(finalMonth),
                            Integer.parseInt(finalDay), Integer.parseInt(myhour), Integer.parseInt(myMinute),
                            Integer.parseInt(mySecond), 1234, fromTimeZone);

            ZonedDateTime currentETime = currentISTime.withZoneSameInstant(toTimeZone);

            //Format date time - optional
            System.out.println("from " + formatter.format(currentISTime));
            System.out.println("to " + formatter.format(currentETime));


            String dtStart = formatter.format(currentETime).substring(10, 20);


            holder.endTime.setText(dtStart);


        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onpayclick.onPayClickClicklistenerteacher(position, teacherHomePageBodyItemArrayList, 1, timerModel);
            }
        });
        if (teacherHomePageBodyItemArrayList.get(position).getLearner_description() != null &&
                !teacherHomePageBodyItemArrayList.get(position).getLearner_description().isEmpty() &&
                !teacherHomePageBodyItemArrayList.get(position).getLearner_description().equals("0")) {
            holder.tvSeeGoals.setVisibility(View.VISIBLE);
        }
        holder.tvSeeGoals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog = new Dialog(context);
                mDialog.setContentView(R.layout.see_learning_goals);
                mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                mDialog.show();

                TextView tvLearningGoals = mDialog.findViewById(R.id.tvLearningGoals);
                Button btDone = mDialog.findViewById(R.id.btDone);

                if (teacherHomePageBodyItemArrayList.get(position).getLearner_description() != null &&
                        !teacherHomePageBodyItemArrayList.get(position).getLearner_description().isEmpty() &&
                        !teacherHomePageBodyItemArrayList.get(position).getLearner_description().equals("0")) {
                    tvLearningGoals.setText(String.valueOf(teacherHomePageBodyItemArrayList.get(position).getLearner_description()));
                }

                btDone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();
                    }
                });


            }
        });
    }


//    public static long getTimeInMillis(int hour, int minute, int seconds) {
//        Calendar calendar = Calendar.getInstance();
//        calendar.set(hour, minute, seconds);
//        return calendar.getTimeInMillis();
//    }

    @Override
    public int getItemCount() {

        return teacherHomePageBodyItemArrayList.size();

    }

    public void setServerTime(String currentdateTime) {
        serverTime = currentdateTime;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView profile;
        RelativeLayout rlPersonImage;
        TextView personName, textCounter, textStatus, nobookingyet;
        LinearLayout llDateTime, bookedlayout;
        View view, mainView;
        CardView cardView;
        TextView time, paymentamount, endTime, date, tvScheduleBooking, tvAccept, tvReject, tvSeeGoals, tvGoals;
        LinearLayout starLayout, llCurruntNotification;


        public ViewHolder(final View itemView) {
            super(itemView);

            rlPersonImage = itemView.findViewById(R.id.rlPersonImage);
            tvGoals = itemView.findViewById(R.id.tvGoals);
            tvSeeGoals = itemView.findViewById(R.id.tvSeeGoals);
            tvAccept = itemView.findViewById(R.id.tvAccept);
            tvReject = itemView.findViewById(R.id.tvReject);
            personName = itemView.findViewById(R.id.personName);
            llDateTime = itemView.findViewById(R.id.llDateTime);
            textStatus = itemView.findViewById(R.id.textStatus);
            tvScheduleBooking = itemView.findViewById(R.id.tvScheduleBooking);
            llCurruntNotification = itemView.findViewById(R.id.llCurruntNotification);
            textCounter = itemView.findViewById(R.id.textCounter);
            view = itemView.findViewById(R.id.view);
            mainView = itemView.findViewById(R.id.mainView);
            cardView = itemView.findViewById(R.id.cardView);
            time = itemView.findViewById(R.id.time);
            paymentamount = itemView.findViewById(R.id.paymentamount);
            starlayout = itemView.findViewById(R.id.starLayout);
            profile = itemView.findViewById(R.id.iv_profile);
            endTime = itemView.findViewById(R.id.endTime);
            date = itemView.findViewById(R.id.date);
            nobookingyet = itemView.findViewById(R.id.nobookingyet);
            bookedlayout = itemView.findViewById(R.id.bookedlayout);
            starLayout = itemView.findViewById(R.id.starLayout);


        }


    }

    public interface onpayclick {
        void onPayClickClicklistenerteacher(int position, ArrayList<TeacherHomePageBodyItem> learnerHomePageBodyItemArrayList, int i, TimerModel timerModel);

        void hitCompletedBookingApi(int position, String status, int booking_id);

        void onAcceptRequest(int position, String s, int booking_id, Integer learnerId);

        void onRejectRequest(int position, String s, int booking_id, Integer learnerId);
    }


}
