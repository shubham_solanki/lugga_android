package com.Lugga.lugga.adapters

import android.content.Context
import android.content.pm.PackageManager
import android.hardware.Camera
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.Lugga.lugga.R
import com.Lugga.lugga.adapters.ImageChooserRecylerViewAdap.ViewHold
import com.Lugga.lugga.interfaces.Interface_ImageChooser
import com.Lugga.lugga.interfaces.onClickImageChooserInterface
import com.Lugga.lugga.utils.CameraPreview
import com.Lugga.lugga.views.ChatActivity
import com.bumptech.glide.Glide
import com.makeramen.roundedimageview.RoundedImageView
import java.util.*

class ImageChooserRecylerViewAdap(chatActivity: ChatActivity?, private val context: Context, private val listOfAllImages: ArrayList<String?>) : RecyclerView.Adapter<ViewHold>() {
    private val chatActivity: ChatActivity? = null
    private var cameraPreview2: CameraPreview? = null
    private var onClickImageChooserinterface: onClickImageChooserInterface? = null
    private var camera: Camera? = null
    private var interface_imageChooser: Interface_ImageChooser? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHold {
        val view = LayoutInflater.from(context).inflate(R.layout.horizontal_view_chooser, parent, false)
        return ViewHold(view)
    }

    override fun onBindViewHolder(holder: ViewHold, position: Int) {
        if (holder.checkCameraHardware() && position == 0) {
            camera = holder.cameraInstance
            if (cameraPreview2 == null) cameraPreview2 = CameraPreview(context, camera)
            if (camera != null) {
                holder.camera_preview?.addView(cameraPreview2)
                holder.camera_preview?.visibility = View.VISIBLE
                holder.cardview?.visibility = View.VISIBLE
            }
        } else {
            holder.camera_preview.removeAllViews()
            holder.camera_preview.visibility = View.GONE
            holder.cardview.visibility = View.GONE
            holder.itemView.setOnClickListener { interface_imageChooser!!.OnSelectedImage(listOfAllImages[position]) }
        }

        //  holder.roundedImageView.setImageURI(Uri.parse(listOfAllImages.get(position)));
        Glide.with(context).load(listOfAllImages[position]).into(holder.roundedImageView)
    }

    override fun getItemCount(): Int {
        return listOfAllImages.size
    }

    inner class ViewHold(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var roundedImageView: RoundedImageView
        var camera_photo: RoundedImageView? = null
        var camera_preview: LinearLayout
        var cardview: CardView
        var c: Camera? = null

        /** Check if this device has a camera  */
        fun checkCameraHardware(): Boolean {
            // this device has a camera
            // no camera on this device
            return context.packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)
        }// Camera is not available (in use or does not exist)
        // returns null if camera is unavailable
// attempt to get a Camera instance

        /** A safe way to get an instance of the Camera object.  */
        val cameraInstance: Camera?
            get() {
                if (c == null) {
                    try {
                        c = Camera.open() // attempt to get a Camera instance
                    } catch (e: Exception) {
                    //    Log.d("lkjfljflfjlflj", e.message)
                        // Camera is not available (in use or does not exist)
                    }
                }
                return c // returns null if camera is unavailable
            }

        override fun onClick(v: View) {
            if (v === cardview) {
                onClickImageChooserinterface!!.onClickImageChooser("Manpreet Dhaliwal", camera_photo)
            }
        }

        private fun releaseCamera() {
            if (camera != null) {
                camera!!.release() // release the camera for other applications
                camera = null
                cameraPreview2 = null
            }
        }

        init {
            roundedImageView = itemView.findViewById(R.id.roundedImageView)
            camera_preview = itemView.findViewById(R.id.camera_preview)
            cardview = itemView.findViewById(R.id.cardview)
            cardview.setOnClickListener(this)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return super.getItemViewType(position)
    }

    fun performClick(onClickImageChooserinterface: onClickImageChooserInterface?) {
        this.onClickImageChooserinterface = onClickImageChooserinterface
    }

    fun setOnImageSelectListner(interface_imageChooser: Interface_ImageChooser?) {
        this.interface_imageChooser = interface_imageChooser
    }

    init {
        Collections.reverse(listOfAllImages)
    }
}