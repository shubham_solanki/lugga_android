package com.Lugga.lugga.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.Lugga.lugga.HomeActivity
import com.Lugga.lugga.R

class PaymentSuccessFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.frag_payment_success, container, false)
        val tvTransID = view.findViewById<TextView>(R.id.tvTransID)
        val tvGoToHome = view.findViewById<TextView>(R.id.tvGoToHome)
        if (arguments!!.getString("transId") != null) {
            tvTransID.text = "Transaction Id : " + arguments!!.getString("transId")
        }
        tvGoToHome.setOnClickListener {
            val intent = Intent(activity, HomeActivity::class.java)
            startActivity(intent)
            activity!!.finish()
        }
        return view
    }
}