package com.Lugga.lugga.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.Lugga.lugga.R

/**
 * A simple [Fragment] subclass.
 */
class PaymentConfirmedFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return LayoutInflater.from(container!!.context).inflate(R.layout.payment_confirmed, container, false)
    }
}