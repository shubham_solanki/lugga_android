package com.Lugga.lugga.views

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.Lugga.lugga.R
import com.Lugga.lugga.model.learnerhomepage.LearnerHomePageBodyItem
import java.util.*

class ConfirmDetailActivity : AppCompatActivity() {

    var intentData: Intent? = null
    var position = 0
    var learnerHomePageBodyItemArrayList: ArrayList<LearnerHomePageBodyItem>? = null
    var name: TextView? = null
    var langauge: TextView? = null
    var level: TextView? = null
    var totalprice: TextView? = null
    var rating: TextView? = null
    var level2: TextView? = null
    var sessiontime: TextView? = null
    var sessionprice: TextView? = null
    var sessiontime2: TextView? = null
    var RequestSendLayout: RelativeLayout? = null
    var ivBack: ImageView? = null
    private var finalAmount = 0

    // String scheduleStartTime,scheduleEndTime;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.confirm_detail_teacher)
        ivBack = findViewById(R.id.ivBack)
        val intent = intent
        //        scheduleStartTime = intent.getStringExtra("scheduleStartTime");
//        scheduleEndTime = intent.getStringExtra("scheduleEndTime");
        ivBack?.setOnClickListener(View.OnClickListener { onBackPressed() })
        init()
        setTextinlayout()
    }

    private fun init() {
        intentData = getIntent()
        learnerHomePageBodyItemArrayList = intentData?.getSerializableExtra("learnerlist") as ArrayList<LearnerHomePageBodyItem>
        position = intentData?.getIntExtra("position", 0)!!
        name = findViewById(R.id.AvineTextView)
        langauge = findViewById(R.id.tv_Learnlanguage)
        level = findViewById(R.id.tv_level)
        totalprice = findViewById(R.id.tv_totalPrice)
        rating = findViewById(R.id.tvRating)
        level2 = findViewById(R.id.tvLevel2)
        sessionprice = findViewById(R.id.tvSessionPrice)
        sessiontime = findViewById(R.id.tvSessiontime)
        sessiontime2 = findViewById(R.id.tv_sessiontime)
        RequestSendLayout = findViewById(R.id.RequestSendLayout)
    }

    private fun setTextinlayout() {
        sessiontime2!!.text = learnerHomePageBodyItemArrayList!![position].getSessionTime()
        name!!.text = learnerHomePageBodyItemArrayList!![position].getName()
        langauge!!.text = learnerHomePageBodyItemArrayList!![position].getLanguage()
        level!!.text = learnerHomePageBodyItemArrayList!![position].getLevel()
        level2!!.text = learnerHomePageBodyItemArrayList!![position].getLevel()
        when (learnerHomePageBodyItemArrayList!![position].getSessionTime()) {
            "30" -> finalAmount = learnerHomePageBodyItemArrayList!![position].amount
            "60" -> finalAmount = learnerHomePageBodyItemArrayList!![position].amount * 2
            "90" -> finalAmount = learnerHomePageBodyItemArrayList!![position].amount * 3
            "120" -> finalAmount = learnerHomePageBodyItemArrayList!![position].amount * 4
        }
        totalprice!!.text = "Total  : $$finalAmount"
        rating!!.text = String.format("%.1f", learnerHomePageBodyItemArrayList!![position].total_rating)
        //        sessiontime.setText(learnerHomePageBodyItemArrayList.get(position).getSessionTime());
        //    sessionprice.setText(learnerHomePageBodyItemArrayList.get(position).getPaymentAmount());
        RequestSendLayout!!.setOnClickListener {
            val intent = Intent(this@ConfirmDetailActivity, PayementActivity::class.java)
            intent.putExtra("learnerlist", learnerHomePageBodyItemArrayList)
            intent.putExtra("position", learnerHomePageBodyItemArrayList)
            intent.putExtra("amount", finalAmount)
            intent.putExtra("bookingId", learnerHomePageBodyItemArrayList!![position].getBookingId())
            intent.putExtra("startTime", learnerHomePageBodyItemArrayList!![position].getStartSession())
            intent.putExtra("endTime", learnerHomePageBodyItemArrayList!![position].getEndSession())
            intent.putExtra("booking_type", learnerHomePageBodyItemArrayList!![position].getBookingType())
            intent.putExtra("stripe_user_id", learnerHomePageBodyItemArrayList!![position].stripe_user_id)
            intent.putExtra("sessionTime", learnerHomePageBodyItemArrayList!![position].getSessionTime())
            intent.putExtra("teacherId", learnerHomePageBodyItemArrayList!![position].getTeacherId().toString())
            //                intent.putExtra("scheduleStartTime",scheduleStartTime);
//                intent.putExtra("scheduleEndTime",scheduleEndTime);
            startActivity(intent)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }
}