package com.Lugga.lugga.views.subscription

import android.app.ProgressDialog
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Paint
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.Lugga.lugga.HomeActivity
import com.Lugga.lugga.R
import com.Lugga.lugga.sharedpreferences.UsersharedPrefernce
import com.Lugga.lugga.utils.sub.IabBroadcastReceiver
import com.Lugga.lugga.utils.sub.IabBroadcastReceiver.IabBroadcastListener
import com.Lugga.lugga.utils.sub.IabHelper
import com.Lugga.lugga.utils.sub.IabHelper.*
import com.Lugga.lugga.utils.sub.Purchase
import com.Lugga.lugga.views.Constants
import kotlinx.android.synthetic.main.activity_subscription.*
import java.util.*


class Subscription : AppCompatActivity(), View.OnClickListener, IabBroadcastListener {
    private var usersharedPrefernce: UsersharedPrefernce? = null
    private var skuList: ArrayList<String>? = null
    private var isSubscribed = false
    private var isAutoRenewable = false
    var progressDialog: ProgressDialog? = null
    var mActiveSub = ""
    var mSelectedSub = Constants.KEY_BRONZE
    private var mHelper: IabHelper? = null
    private var mReceiver: IabBroadcastReceiver? = null
    private var packagePriceTxt: TextView? = null
    private var packageBenefitTxt: TextView? = null
    private var packageDurationTxt: TextView? = null
    private var packageTypeImg: ImageView? = null
    private var subBackPress: ImageView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_subscription)
        packagePriceTxt = findViewById(R.id.textView_package_price)
        packageBenefitTxt = findViewById(R.id.textView_package_benefits)
        packageDurationTxt = findViewById(R.id.textView_package_duration)
        packageTypeImg = findViewById(R.id.imageView_package_type)
        subBackPress = findViewById(R.id.subBackPress)
        findViewById<View>(R.id.button_bronze).setOnClickListener(this)
        findViewById<View>(R.id.button_silver).setOnClickListener(this)
        findViewById<View>(R.id.button_gold).setOnClickListener(this)
        findViewById<View>(R.id.button_buy_now).setOnClickListener(this)
        subBackPress?.setOnClickListener(View.OnClickListener { onBackPressed() })
        usersharedPrefernce = UsersharedPrefernce.getInstance()
        initSubscription()

        packagePriceTxt!!.paintFlags = packagePriceTxt!!.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG

    }

    public override fun onDestroy() {
        super.onDestroy()
        if (mReceiver != null) {
            unregisterReceiver(mReceiver)
        }
        if (mHelper != null) {
            mHelper!!.disposeWhenFinished()
            mHelper = null
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.button_bronze -> {
                packagePriceTxt!!.paintFlags = packagePriceTxt!!.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                tvFree.visibility=View.VISIBLE
                tvFreeThreeMonths.visibility=View.VISIBLE
                packageDurationTxt!!.text = "Free Trial (3 months)"
                packageTypeImg!!.setImageResource(R.drawable.bronze)
                mSelectedSub = Constants.KEY_BRONZE
                unregisterReceiver()
                initSubscription()
            }
            R.id.button_gold -> {
                packagePriceTxt!!.paintFlags = 0
                tvFree.visibility=View.GONE
                tvFreeThreeMonths.visibility=View.GONE
                packageDurationTxt!!.text = "1 year"
                packageTypeImg!!.setImageResource(R.drawable.gold)
                mSelectedSub = Constants.KEY_GOLD
                unregisterReceiver()
                initSubscription()
            }
            R.id.button_silver -> {
                packagePriceTxt!!.paintFlags = 0
                tvFree.visibility=View.GONE
                tvFreeThreeMonths.visibility=View.GONE
                packageDurationTxt!!.text = "6 months"
                mSelectedSub = Constants.KEY_SILVER
                packageTypeImg!!.setImageResource(R.drawable.silver)
                unregisterReceiver()
                initSubscription()
            }
            R.id.button_buy_now -> startPurchaseFlow()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.d(TAG, "onActivityResult($requestCode,$resultCode,$data")
        if (mHelper == null) return

        // Pass on the activity result to the helper for handling
        if (!mHelper!!.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data)
        } else {
            Log.d(TAG, "onActivityResult handled by IABUtil.")
        }
    }

    private fun unregisterReceiver() {
        if (mReceiver != null) {
            unregisterReceiver(mReceiver)
        }
        if (mHelper != null) {
            mHelper!!.disposeWhenFinished()
            mHelper = null
        }
    }

    private fun initSubscription() {
        skuList = ArrayList()
        skuList!!.add(Constants.KEY_BRONZE)
        skuList!!.add(Constants.KEY_GOLD)
        skuList!!.add(Constants.KEY_SILVER)
        mHelper = IabHelper(this, Constants.BASE64_LICENSE)
        mHelper!!.enableDebugLogging(true)
        mHelper!!.startSetup(OnIabSetupFinishedListener { result ->
            if (!result.isSuccess) {
                Log.d(TAG, "IabHelper fail")
            }
            if (mHelper == null) return@OnIabSetupFinishedListener
            mReceiver = IabBroadcastReceiver(this@Subscription)
            val broadcastFilter = IntentFilter(IabBroadcastReceiver.ACTION)
            registerReceiver(mReceiver, broadcastFilter)

            // IAB is fully set up. Now, let's get an inventory of stuff we own.
            try {
                mHelper!!.queryInventoryAsync(skuList, mGotInventoryListener)
            } catch (e: IabAsyncInProgressException) {
                Log.d(TAG, "Exception: $e")
            }
        })
    }

    private fun startPurchaseFlow() {
        val payload = ""
        var oldSkus: MutableList<String?>? = null
        if (!TextUtils.isEmpty(mActiveSub)
                && mActiveSub != mSelectedSub) {
            // The user currently has a valid subscription, any purchase action is going to
            // replace that subscription
            oldSkus = ArrayList()
            oldSkus.add(mActiveSub)
        }
        Log.d("Subscription", "Selected Sub: $mSelectedSub")
        try {
            mHelper!!.launchPurchaseFlow(this, mSelectedSub, ITEM_TYPE_SUBS,
                    oldSkus, RC_REQUEST, mPurchaseFinishedListener, payload)
        } catch (e: IabAsyncInProgressException) {
            Log.d(TAG, "Exception: $e")
        }

        // Reset the dialog options
        mSelectedSub = ""
    }

    override fun receivedBroadcast() {
        // Received a broadcast notification that the inventory of items has changed
        Log.d(TAG, "Received broadcast notification. Querying inventory.")
        try {
            mHelper!!.queryInventoryAsync(skuList, mGotInventoryListener)
        } catch (e: IabAsyncInProgressException) {
            Log.d(TAG, "Exception: $e")
        }
    }

    fun verifyDeveloperPayload(p: Purchase): Boolean {
        val payload = p.developerPayload
        return true
    }

    private val mGotInventoryListener = QueryInventoryFinishedListener { result, inventory ->
        Log.d(TAG, "Querying finished")
        if (mHelper == null) return@QueryInventoryFinishedListener
        if (result.isFailure) {
            Log.d(TAG, "Querying fail")
            return@QueryInventoryFinishedListener
        }
        Log.d(TAG, "Query inventory was successful.")
        if (mSelectedSub == Constants.KEY_SILVER) {
            packagePriceTxt!!.text = inventory.getSkuDetails(Constants.KEY_SILVER).price
        } else if (mSelectedSub == Constants.KEY_GOLD) {
            packagePriceTxt!!.text = inventory.getSkuDetails(Constants.KEY_GOLD).price
        } else {
            packagePriceTxt!!.text = inventory.getSkuDetails(Constants.KEY_BRONZE).price
        }

        // First find out which subscription is auto renewing
        val gold = inventory.getPurchase(Constants.KEY_GOLD)
        val silver = inventory.getPurchase(Constants.KEY_SILVER)
        val bronze = inventory.getPurchase(Constants.KEY_BRONZE)
        if (gold != null && gold.isAutoRenewing) {
            mActiveSub = Constants.KEY_GOLD
            isAutoRenewable = true
        } else if (silver != null && silver.isAutoRenewing) {
            mActiveSub = Constants.KEY_SILVER
            isAutoRenewable = true
        } else if (bronze != null && bronze.isAutoRenewing) {
            mActiveSub = Constants.KEY_BRONZE
            isAutoRenewable = true
        } else {
            mActiveSub = ""
            isAutoRenewable = false
        }
        isSubscribed = (gold != null && verifyDeveloperPayload(gold)
                || silver != null && verifyDeveloperPayload(silver)
                || bronze != null && verifyDeveloperPayload(bronze))
        Log.d(TAG, "User " + (if (isSubscribed) "HAS" else "DOES NOT HAVE")
                + " subscription.")
        Log.d(TAG, "Initial inventory query finished; enabling main UI.")

    }

    // Callback for when a purchase is finished
    var mPurchaseFinishedListener = OnIabPurchaseFinishedListener { result, purchase ->

        Log.d(TAG, "Purchase finished: $result, purchase: $purchase")

        // if we were disposed of in the meantime, quit.
        if (mHelper == null) return@OnIabPurchaseFinishedListener
        if (result.isFailure) {
            return@OnIabPurchaseFinishedListener
        }
        if (!verifyDeveloperPayload(purchase)) {
            return@OnIabPurchaseFinishedListener
        }

        Log.d(TAG, "Purchase successful.")
        if (purchase.sku == Constants.KEY_BRONZE || purchase.sku == Constants.KEY_GOLD || purchase.sku == Constants.KEY_SILVER) {
            Log.d(TAG, "Subscription purchased.")
            isSubscribed = true
            isAutoRenewable = purchase.isAutoRenewing
            mActiveSub = purchase.sku
            isShowProgress
            Handler().postDelayed({
                usersharedPrefernce!!.setboolean(true)
                isDismiss
                val intent = Intent(this@Subscription, HomeActivity::class.java)
                startActivity(intent)
                finishAffinity()
            }, 1500)
        }
    }

    val isShowProgress: Unit
        get() {
            if (progressDialog == null) {
                progressDialog = ProgressDialog(this@Subscription)
                progressDialog!!.setTitle("Loading")
                progressDialog!!.setMessage("wait while loading...")
                progressDialog!!.setCancelable(false)
            }
            progressDialog!!.show()
        }

    val isDismiss: Unit
        get() {
            if (progressDialog != null && progressDialog!!.isShowing) {
                progressDialog!!.dismiss()
            }
        }

    companion object {
        private const val TAG = "Subscription"
        private const val RC_REQUEST = 10001
    }

}