package com.Lugga.lugga.sharedpreferences;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.Window;
import android.view.WindowManager;

//
//import com.Lugga.lugga.R;
//import com.Lugga.lugga.views.BasicActivity;
//import com.balsikandar.crashreporter.CrashReporter;
//import com.balsikandar.crashreporter.utils.CrashUtil;

import androidx.core.content.ContextCompat;

import com.Lugga.lugga.utils.BackgroundService;
import com.stripe.android.Stripe;

import java.net.Socket;


public class BaseApplication  extends Application {
    static Context context;
    private static Socket socket;

    public static final String TAG = BaseApplication.class.getSimpleName();
    private static BaseApplication mInstance;
    private static Stripe stripe;


    public static synchronized BaseApplication getInstance()
    {
        return mInstance;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        mInstance = this;
        //startService(new Intent(this, BackgroundService.class));
//
//        CrashReporter.initialize(this);
//        CrashUtil.getDefaultPath();
//        try {
//            // Do your stuff
//        } catch (Exception e) {
//            CrashReporter.logException(e);
//        }


    }



    static public Context getContext() {
        return context;
    }

    public static Stripe getStripe(Context context) {
        if(stripe==null)
          // stripe=  new Stripe(context, "pk_test_8Sn4I2zPmPVhUqOdZiijkVLL002gbmqlLa");
            stripe=  new Stripe(context, "pk_live_ebFBDc64Pi0Y0lfrVPaOp1q100AAuI1u3m");

        return stripe;
    }

}
