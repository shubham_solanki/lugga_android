package com.Lugga.lugga.sharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;

public class UsersharedPrefernce {


    private String USERID="userid";
    private String USERTYPE="usertype";
    private String MOBILENO="mobileno";
    private String NAME="name";
    private String EXPERIENCE="experience";
    private String LANGUAGE="language";
    private String LEVEL="level";
    private String PASSWORD="password";
    private String EMAIL="email";
    private String DOCUMENTFILE="documentfile";
    private String COMPLETEDJOB="completedjob";
    private String UNCOMPLETEDJOB="uncompletedjob";
    private String RATING="rating";
    private String PROFILEIMAGE="profileimage";
    private String RECEIVER_PROFILE_IMAGE="receiverprofileimage";

    private String STATUS="status";
    private String AMOUNT="amount";
    private String LEARNERID="learnerid";
    private String TEACHERID="teacherid";
    private String TEACHERNAME="teacherName";
    private String LEARNERNAME="learnername";
    private String STRIPEUSERID="stripeUserId";
    private String TIME="time";
    private String NOTIFICATIONMESSAGE="notificationmessage";
    private String ISONLINE="isonline";
    private String NEWUSER="newuser";
    private String BOOKINGID="bookingId";
    private String IMAGE="image";
    private String DEVICETOKEN="devicetoken";
    private String BOOKING_ID="bookingId";
    private String toogle="toogle";

    SharedPreferences sharedPreferences;
    static UsersharedPrefernce userSharedPrefernce = null;
    Context context;

    public UsersharedPrefernce() {

    }

    public UsersharedPrefernce(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);

    }

    public static UsersharedPrefernce

    getInstance() {
        if (userSharedPrefernce == null) {
            userSharedPrefernce = new UsersharedPrefernce(BaseApplication.getContext());
        }

        return userSharedPrefernce;
    }

    public void setBOOKINGID(int bookingid) {
        sharedPreferences.edit().putInt(BOOKINGID, bookingid).commit();
    }

    public int getBOOKINGId() {
        return sharedPreferences.getInt(BOOKINGID, 0);
    }

    public void setuserid(int userid) {
        sharedPreferences.edit().putInt(USERID, userid).commit();
    }

    public int getuserid() {
        return sharedPreferences.getInt(USERID, 0);
    }


    public void setusertype(int usertype) {
        sharedPreferences.edit().putInt(USERTYPE, usertype).commit();
    }

    public int getusertype() {
        return sharedPreferences.getInt(USERTYPE, 0);
    }

    public void setname(String name){
        sharedPreferences.edit().putString(NAME,name).commit();
    }
    public String getname(){

        return sharedPreferences.getString(NAME,null);
    }

    public void setpassword(String password){
        sharedPreferences.edit().putString(PASSWORD,password).commit();
    }
    public String getpassword(){

        return sharedPreferences.getString(PASSWORD,null);
    }
    public void setmobileno(String mobileno){

        sharedPreferences.edit().putString(MOBILENO,mobileno).commit();
    }
    public String getmobileno(){
        return sharedPreferences.getString(MOBILENO,null);
    }
    public void setexperience(String experience){
        sharedPreferences.edit().putString(EXPERIENCE,experience).commit();
    }
    public String getexperience(){
        return sharedPreferences.getString(EXPERIENCE,null);
    }
    public void setlanguage(String language){
        sharedPreferences.edit().putString(LANGUAGE,language).commit();
    }
    public String getlanguage(){
        return sharedPreferences.getString(LANGUAGE,null);
    }
    public void setlevel(int level){
        sharedPreferences.edit().putInt(LEVEL,level).commit();
    }
    public int getlevel(){
        return sharedPreferences.getInt(LEVEL,0);
    }

    public void setemail(String email){
        sharedPreferences.edit().putString(EMAIL,email).commit();
    }
    public String getemail(){
        return sharedPreferences.getString(EMAIL,null);
    }

    public void setdocumentfile(String documentfile){
        sharedPreferences.edit().putString(DOCUMENTFILE,documentfile).commit();
    }
    public String getdocumentfile(){
        return sharedPreferences.getString(DOCUMENTFILE,null);
    }
    public void setcompletedjob(int completedjob){
        sharedPreferences.edit().putInt(COMPLETEDJOB,completedjob).commit();
    }
    public int getcompletedjob(){
        return sharedPreferences.getInt(COMPLETEDJOB,0);
    }
    public void setuncompletedjob(String uncompletedjob){
        sharedPreferences.edit().putString(UNCOMPLETEDJOB,uncompletedjob).commit();
    }
    public String getuncompletedjob(){
        return sharedPreferences.getString(UNCOMPLETEDJOB,null);
    }

    public void setrating(int rating){
        sharedPreferences.edit().putInt(RATING,rating).commit();
    }
    public int getrating(){
        return sharedPreferences.getInt(RATING,0);
    }

    public void setprofileimage(String profileimage){

        sharedPreferences.edit().putString(PROFILEIMAGE,profileimage).commit();
    }
    public String getprofileimage(){

        return sharedPreferences.getString(PROFILEIMAGE,"");
    }



    public void setReceiverProfileimage(String receiverProfileimage){

        sharedPreferences.edit().putString(RECEIVER_PROFILE_IMAGE,receiverProfileimage).commit();
    }
    public String getReceiverProfileimage(){

        return sharedPreferences.getString(RECEIVER_PROFILE_IMAGE,"");
    }



    public void setamount(String amount){
        sharedPreferences.edit().putString(AMOUNT,amount).commit();
    }
    public String getamount(){
        return sharedPreferences.getString(AMOUNT,null);
    }
    public void setlearnername(String learnername){
        sharedPreferences.edit().putString(LEARNERNAME,learnername).commit();
    }
    public String getlearnername(){
        return sharedPreferences.getString(LEARNERNAME,null);
    }
    public void settechernername(String teachername){
        sharedPreferences.edit().putString(TEACHERNAME,teachername).commit();
    }
    public String getteachername(){
        return sharedPreferences.getString(TEACHERNAME,null);
    }
    public void setnotificationmessage(String notificationmessage){
        sharedPreferences.edit().putString(NOTIFICATIONMESSAGE,notificationmessage).commit();
    }
    public String getnotificationmessage(){
        return sharedPreferences.getString(NOTIFICATIONMESSAGE,null);
    }
    public void setisonline(int isonline){
        sharedPreferences.edit().putInt(ISONLINE,isonline).commit();
    }
    public int getisonline(){
        return sharedPreferences.getInt(ISONLINE,0);
    }

    public void setstatus(int status){
        sharedPreferences.edit().putInt(STATUS,status).commit();
    }
    public int getstatus(){
        return sharedPreferences.getInt(STATUS,0);
    }

    public void settime(String time){
        sharedPreferences.edit().putString(TIME,time).commit();
    }
    public String gettime(){
        return sharedPreferences.getString(TIME,null);
    }
    public void setlearnerid(int learnerid){
        sharedPreferences.edit().putInt(LEARNERID,learnerid).commit();
    }
    public int getlearnerid(){
        return sharedPreferences.getInt(LEARNERID,0);
    }
    public void setteacherid(int teacherid){
        sharedPreferences.edit().putInt(TEACHERID,teacherid).commit();
    }
    public int getteacherid(){
        return sharedPreferences.getInt(TEACHERID,0);
    }
    public void setIMAGE(String image){
        sharedPreferences.edit().putString(IMAGE,image).commit();
    }
    public String getImage(){
        return sharedPreferences.getString(IMAGE," ");
    }
    public void setboolean(boolean value) {
        sharedPreferences.edit().putBoolean(NEWUSER,value).commit();

    }

    public boolean getboolean() {
        return sharedPreferences.getBoolean(NEWUSER,false);
    }

//    public void setString(String loggedin){
//        sharedPreferences.edit().putString(LOGGEDIN,loggedin).commit();
//    }
//    public String getString(String loggedin,String s){
//        return sharedPreferences.getString(LOGGEDIN,null);
//    }


    public void setDEVICETOKEN(String devicetoken){
        sharedPreferences.edit().putString(DEVICETOKEN,devicetoken).commit();
    }
    public String getDEVICETOKEN(){
        return sharedPreferences.getString(DEVICETOKEN,null);
    }


    public void setStripeUserId(String stripeUserId){
        sharedPreferences.edit().putString(STRIPEUSERID,stripeUserId).commit();
    }
    public String getStripeUserId(){
        return sharedPreferences.getString(STRIPEUSERID,null);
    }





    public void setbookingIdCall(String bookingid){
        sharedPreferences.edit().putString(BOOKING_ID,bookingid).commit();
    }
    public String getbookingIdCall(){
        return sharedPreferences.getString(BOOKING_ID,null);
    }


}
