package com.Lugga.lugga.utils;

import android.util.Log;

import com.Lugga.lugga.utils.ApiClient;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;

public class SocketNetworking {

    private static Socket socket;

    public static Socket getSocket() {
        if (socket == null) {
            try {

                socket = IO.socket(ApiClient.BASE_URL);
            } catch (URISyntaxException e) {
                e.printStackTrace();
                Log.d("socketexception", e.getMessage());
            }
        }
        socket.connect();
        return socket;
    }
}


