package com.Lugga.lugga.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.Lugga.lugga.CallReceiveActivity;

public class PollReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent intent1=new Intent(context, CallReceiveActivity.class);
        context.startActivity(intent1);
    }
}
