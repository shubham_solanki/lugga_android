package com.Lugga.lugga.utils;

import android.app.ProgressDialog;
import android.content.Context;

public class Loader {

    private ProgressDialog progressDialog;
    private Context context;

    public Loader(Context context) {
        this.context = context;
    }

    public void isShowProgress(){

        if (progressDialog == null){

            progressDialog=new ProgressDialog(context);
            progressDialog.setTitle("Loading");
            progressDialog.setMessage("wait while loading...");
            progressDialog.setCancelable(false);

        }
        progressDialog.show();

    }
    public void isDismiss(){

        if (progressDialog != null && progressDialog.isShowing()){

            progressDialog.dismiss();
        }
    }
}
