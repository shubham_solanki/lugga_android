package com.Lugga.lugga.utils;

import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

  //public static final String BASE_URL = "http://luggalanguages.com:91/";    //testing
  //  public static final String IMAGE_URL = "http://luggalanguages.com";       //testing
    public static final String BASE_URL = "http://luggalanguages.com:90/";       //live
    public static final String IMAGE_URL = "http://luggalanguages.com";         //live


    private static Retrofit retrofit = null;
    public static Retrofit getApiClient() {
        if (retrofit == null) {
            OkHttpClient client = new OkHttpClient().newBuilder().connectTimeout(160, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .build();
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().serializeNulls().create()))
                    .build();

        }

        return retrofit;
    }


    ////new code h
}
