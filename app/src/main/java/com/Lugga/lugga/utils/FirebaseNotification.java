package com.Lugga.lugga.utils;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.os.Build;
import android.os.PowerManager;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;

import com.Lugga.lugga.CallReceiveActivity;
import com.Lugga.lugga.NewCallReceiveActivity;
import com.Lugga.lugga.R;
import com.Lugga.lugga.sharedpreferences.UsersharedPrefernce;
import com.Lugga.lugga.HomeActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import io.socket.client.Socket;

import static com.Lugga.lugga.sharedpreferences.BaseApplication.getContext;


public class FirebaseNotification extends FirebaseMessagingService {
    private static final String TAG = "FCM Service";
    private static int count = 0;
    private JSONObject data;
    UsersharedPrefernce usersharedPrefernce;
    public static final String MyPREFERENCES = "MyPrefs";
    private SharedPreferences sharedpreferences;
    private NotificationManager mNotificationManager;
    Intent notiIntent;
    private NotificationCompat.Builder notificationBuilder;
    public static final int VISIBILITY_PUBLIC = -1;
    private Intent intentNew;
    private Socket socket;

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        usersharedPrefernce = UsersharedPrefernce.getInstance();
        usersharedPrefernce.setDEVICETOKEN(s);
        SharedPreferences.Editor editor = getSharedPreferences("TokenPref", MODE_PRIVATE).edit();
        editor.putString("token", s);
        editor.apply();
    }


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        if (remoteMessage.getData().get("message") != null) {

            Log.e("not", String.valueOf(remoteMessage.getData()));
            Log.e("noti", remoteMessage.getData().get("message"));

            socket = SocketNetworking.getSocket();
            socket.connect();

            usersharedPrefernce = UsersharedPrefernce.getInstance();
            if (remoteMessage.getData().size() > 0) {
                JSONObject channel_id = null;

                try {
                    channel_id = new JSONObject(remoteMessage.getData().get("message"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                mNotificationManager = (NotificationManager)
                        getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

                try {
                    if (channel_id != null && channel_id.getString("caller_id") != null) {
                        notiIntent = new Intent(this, NewCallReceiveActivity.class);
                        notiIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        notiIntent.putExtra("channel_id", channel_id.getString("channel_id"));
                        notiIntent.putExtra("caller_id", channel_id.getString("caller_id"));
                        notiIntent.putExtra("receiver_id", channel_id.getString("receiver_id"));
                        notiIntent.putExtra("caller_name", channel_id.getString("caller_name"));
                        notiIntent.putExtra("user_type", channel_id.getString("user_type"));
                        notiIntent.putExtra("call_type", channel_id.getString("call_type"));
                        notiIntent.putExtra("sender_name", channel_id.getString("sender_name"));
                        notiIntent.putExtra("booking_id", channel_id.getString("booking_id"));
                        startActivity(notiIntent);
                    } else {
                        intentNew = new Intent(this, HomeActivity.class);
                        setNotification(remoteMessage.getData().get("message"));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }


        }

    }

    public void setNotification(String message) {

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(intentNew);
        PendingIntent resultPendingIntent2 = stackBuilder.getPendingIntent(12, PendingIntent.FLAG_UPDATE_CURRENT);

//        Uri soundUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" +
//                getApplicationContext().getPackageName() + "/" + R.raw.incoming_tone);
        String channelId = getString(R.string.default_notification_channel_id);
        notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.splash_screen_teacher)
                        .setColor(getResources().getColor(R.color.colorPrimary))
                        .setContentTitle("Lugga")

                        .setContentText(message)
                        .setAutoCancel(true)
                        .setFullScreenIntent(resultPendingIntent2, true)
                        .setVisibility(VISIBILITY_PUBLIC)
                        .setOnlyAlertOnce(true)
                        .setContentIntent(resultPendingIntent2);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Lugga",
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setLightColor(Color.GRAY);
            channel.enableLights(true);

            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build();
            channel.enableVibration(true);

            if (mNotificationManager != null) {
                mNotificationManager.createNotificationChannel(channel);
            }
        }

        mNotificationManager.notify(12 /* ID of notification */, notificationBuilder.build());

        JSONObject jsonObject = new JSONObject();

        try {
            if (usersharedPrefernce.getusertype() == 2) {
                jsonObject.put("id", usersharedPrefernce.getuserid());
                jsonObject.put("user_type", "2");
            } else {
                jsonObject.put("id", usersharedPrefernce.getuserid());
                jsonObject.put("user_type", "1");
            }
            socket.emit("HomepageSocket", jsonObject);
        } catch (JSONException e) {

        }


//        wakeUpScreen();
//        String ns = Context.NOTIFICATION_SERVICE;
//        NotificationManager nMgr = (NotificationManager) this.getSystemService(ns);
//        nMgr.cancel(12);
    }


}