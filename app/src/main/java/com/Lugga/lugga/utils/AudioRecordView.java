package com.Lugga.lugga.utils;

import android.animation.Animator;
import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.widget.TextViewCompat;

import com.Lugga.lugga.R;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Varun John on 4 Dec, 2018
 * Github : https://github.com/varunjohn
 */
public class AudioRecordView {
    private static final String AUDIO_RECORDER_FILE_EXT_3GP = ".3gp";
    private static final String AUDIO_RECORDER_FILE_EXT_MP4 = ".mp4";
    private static final String AUDIO_RECORDER_FOLDER = "AudioRecorder";
    private static int CLICK_THRESHOLD = 100;
    int second = -1, minute, hour;

    private MediaRecorder recorder = null;
    private int currentFormat = 0;
    private int output_formats[] = {MediaRecorder.OutputFormat.MPEG_4, MediaRecorder.OutputFormat.THREE_GPP};
    private String file_exts[] = {AUDIO_RECORDER_FILE_EXT_MP4, AUDIO_RECORDER_FILE_EXT_3GP};
    private String fileName = "";
    private CountDownTimer countDownTimer;
    private boolean isYes;
    private String AudioSavePathInDevice=null;
    MediaRecorder mediaRecorder ;
    Random random ;
    String RandomAudioFileName = "ABCDEFGHIJKLMNOP";
    public static final int RequestPermissionCode = 1;
    MediaPlayer mediaPlayer;

    public enum UserBehaviour {
        CANCELING,
        LOCKING,
        NONE
    }

    public enum RecordingBehaviour {
        CANCELED,
        LOCKED,
        LOCK_DONE,
        RELEASED
    }

    public interface RecordingListener {

        void onRecordingStarted();

        void onRecordingLocked();

        void onRecordingCompleted(String fileName);

        void onRecordingCanceled();

    }

    private String TAG = "AudioRecordView";
    String filePath;
    String audioFile;
    private LinearLayout viewContainer, layoutAttachmentOptions;
    private View imageViewAudio, imageViewLockArrow, imageViewLock, imageViewMic, dustin, dustin_cover, imageViewStop, imageViewSend;
    private View layoutAttachment, layoutDustin, layoutMessage, imageViewCamera;
    private View layoutSlideCancel, layoutLock, layoutEffect1, layoutEffect2;
    private EditText editTextMessage;
    private TextView timeText, textViewSlide;

    private ImageView stop, audio, send;

    private Animation animBlink, animJump, animJumpFast;

    private boolean isDeleting;
    private boolean stopTrackingAction;
    private Handler handler;

    private int audioTotalTime;
    private TimerTask timerTask;
    private Timer audioTimer;
    private SimpleDateFormat timeFormatter;

    private float lastX, lastY;
    private float firstX, firstY;

    private float directionOffset, cancelOffset, lockOffset;
    private float dp = 0;
    private boolean isLocked = false;

    private UserBehaviour userBehaviour = UserBehaviour.NONE;
    private RecordingListener recordingListener;

    boolean isLayoutDirectionRightToLeft;

    int screenWidth, screenHeight;

    private List<AttachmentOption> attachmentOptionList;
    private AttachmentOptionsListener attachmentOptionsListener;

    private List<LinearLayout> layoutAttachments;

    private Context context;

    private boolean showCameraIcon = true, showAttachmentIcon = true, showEmojiIcon = true;
    private boolean removeAttachmentOptionAnimation;

    public void initView(ViewGroup view) {

        if (view == null) {
            showErrorLog("initView ViewGroup can't be NULL");
            return;
        }
        random = new Random();

        context = view.getContext();

        view.removeAllViews();
        view.addView(LayoutInflater.from(view.getContext()).inflate(R.layout.activity_new_chat, null));

        timeFormatter = new SimpleDateFormat("m:ss", Locale.getDefault());

        DisplayMetrics displayMetrics = view.getContext().getResources().getDisplayMetrics();
        screenHeight = displayMetrics.heightPixels;
        screenWidth = displayMetrics.widthPixels;

        isLayoutDirectionRightToLeft = view.getContext().getResources().getBoolean(R.bool.is_right_to_left);

        viewContainer = view.findViewById(R.id.layoutContainer);
        layoutAttachmentOptions = view.findViewById(R.id.layoutAttachmentOptions);


        imageViewCamera = view.findViewById(R.id.imageViewCamera);

        editTextMessage = view.findViewById(R.id.editTextMessage);

        send = view.findViewById(R.id.imageSend);
        stop = view.findViewById(R.id.imageStop);
        audio = view.findViewById(R.id.imageAudio);

        imageViewAudio = view.findViewById(R.id.imageViewAudio);
        imageViewStop = view.findViewById(R.id.imageViewStop);
        imageViewSend = view.findViewById(R.id.imageViewSend);
        imageViewLock = view.findViewById(R.id.imageViewLock);
        imageViewLockArrow = view.findViewById(R.id.imageViewLockArrow);
        layoutDustin = view.findViewById(R.id.layoutDustin);
        layoutMessage = view.findViewById(R.id.layoutMessage);
        layoutAttachment = view.findViewById(R.id.layoutAttachment);
        textViewSlide = view.findViewById(R.id.textViewSlide);
        timeText = view.findViewById(R.id.textViewTime);
        layoutSlideCancel = view.findViewById(R.id.layoutSlideCancel);
        layoutEffect2 = view.findViewById(R.id.layoutEffect2);
        layoutEffect1 = view.findViewById(R.id.layoutEffect1);
        layoutLock = view.findViewById(R.id.layoutLock);
        imageViewMic = view.findViewById(R.id.imageViewMic);
        dustin = view.findViewById(R.id.dustin);
        dustin_cover = view.findViewById(R.id.dustin_cover);

        handler = new Handler(Looper.getMainLooper());

        dp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1, view.getContext().getResources().getDisplayMetrics());

        animBlink = AnimationUtils.loadAnimation(view.getContext(),
                R.anim.blink);
        animJump = AnimationUtils.loadAnimation(view.getContext(),
                R.anim.jump);
        animJumpFast = AnimationUtils.loadAnimation(view.getContext(),
                R.anim.jump_fast);

        setupRecording();
    }

    public void changeSlideToCancelText(int textResourceId) {
        textViewSlide.setText(textResourceId);
    }

    public boolean isShowCameraIcon() {
        return showCameraIcon;
    }

    public void showCameraIcon(boolean showCameraIcon) {
        this.showCameraIcon = showCameraIcon;

        if (showCameraIcon) {
            imageViewCamera.setVisibility(View.VISIBLE);
        } else {
            imageViewCamera.setVisibility(View.INVISIBLE);
        }
    }

    public boolean isShowAttachmentIcon() {
        return showAttachmentIcon;
    }

    public void showAttachmentIcon(boolean showAttachmentIcon) {
        this.showAttachmentIcon = showAttachmentIcon;


    }

    public boolean isShowEmojiIcon() {
        return showEmojiIcon;
    }

    public void showEmojiIcon(boolean showEmojiIcon) {
        this.showEmojiIcon = showEmojiIcon;


    }

    public void setAttachmentOptions(List<AttachmentOption> attachmentOptionList, final AttachmentOptionsListener attachmentOptionsListener) {

        this.attachmentOptionList = attachmentOptionList;
        this.attachmentOptionsListener = attachmentOptionsListener;

        if (this.attachmentOptionList != null && !this.attachmentOptionList.isEmpty()) {
            layoutAttachmentOptions.removeAllViews();
            int count = 0;
            LinearLayout linearLayoutMain = null;
            layoutAttachments = new ArrayList<>();

            for (final AttachmentOption attachmentOption : this.attachmentOptionList) {

                if (count == 6) {
                    break;
                }

                if (count == 0 || count == 3) {
                    linearLayoutMain = new LinearLayout(context);
                    linearLayoutMain.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    linearLayoutMain.setOrientation(LinearLayout.HORIZONTAL);
                    linearLayoutMain.setGravity(Gravity.CENTER);

                    layoutAttachmentOptions.addView(linearLayoutMain);
                }

                LinearLayout linearLayout = new LinearLayout(context);
                linearLayout.setLayoutParams(new LinearLayout.LayoutParams((int) (dp * 84), LinearLayout.LayoutParams.WRAP_CONTENT));
                linearLayout.setPadding((int) (dp * 4), (int) (dp * 12), (int) (dp * 4), (int) (dp * 0));
                linearLayout.setOrientation(LinearLayout.VERTICAL);
                linearLayout.setGravity(Gravity.CENTER);

                layoutAttachments.add(linearLayout);

                ImageView imageView = new ImageView(context);
                imageView.setLayoutParams(new LinearLayout.LayoutParams((int) (dp * 48), (int) (dp * 48)));
                imageView.setImageResource(attachmentOption.getResourceImage());

                TextView textView = new TextView(context);
                TextViewCompat.setTextAppearance(textView, R.style.TextAttachmentOptions);
                textView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                textView.setPadding((int) (dp * 4), (int) (dp * 4), (int) (dp * 4), (int) (dp * 0));
                textView.setMaxLines(1);
                textView.setText(attachmentOption.getTitle());

                linearLayout.addView(imageView);
                linearLayout.addView(textView);

                linearLayoutMain.addView(linearLayout);

                linearLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        hideAttachmentOptionView();
                        AudioRecordView.this.attachmentOptionsListener.onClick(attachmentOption);
                    }
                });

                count++;
            }
        }
    }

    public void hideAttachmentOptionView() {
        if (layoutAttachment.getVisibility() == View.VISIBLE) {

        }
    }

    public void showAttachmentOptionView() {
        if (layoutAttachment.getVisibility() != View.VISIBLE) {

        }
    }


    public void removeAttachmentOptionAnimation(boolean removeAttachmentOptionAnimation) {
        this.removeAttachmentOptionAnimation = removeAttachmentOptionAnimation;
    }

    public View setContainerView(int layoutResourceID) {
        View view = LayoutInflater.from(viewContainer.getContext()).inflate(layoutResourceID, null);

        if (view == null) {
            showErrorLog("Unable to create the Container View from the layoutResourceID");
            return null;
        }

        viewContainer.removeAllViews();
        viewContainer.addView(view);
        return view;
    }

    public void setAudioRecordButtonImage(int imageResource) {
        audio.setImageResource(imageResource);
    }

    public void setStopButtonImage(int imageResource) {
        stop.setImageResource(imageResource);
    }

    public void setSendButtonImage(int imageResource) {
        send.setImageResource(imageResource);
    }

    public RecordingListener getRecordingListener() {
        return recordingListener;
    }

    public void setRecordingListener(RecordingListener recordingListener) {
        this.recordingListener = recordingListener;
    }

    public View getSendView() {
        return imageViewSend;
    }


    public View getCameraView() {
        return imageViewCamera;
    }

    public EditText getMessageView() {
        return editTextMessage;
    }

    private void setupRecording() {

        imageViewSend.animate().scaleX(0f).scaleY(0f).setDuration(100).setInterpolator(new LinearInterpolator()).start();

        editTextMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().trim().isEmpty()) {
                    if (imageViewSend.getVisibility() != View.GONE) {
                        imageViewSend.setVisibility(View.GONE);
                        imageViewSend.animate().scaleX(0f).scaleY(0f).setDuration(100).setInterpolator(new LinearInterpolator()).start();
                    }

                    if (showCameraIcon) {
                        if (imageViewCamera.getVisibility() != View.VISIBLE && !isLocked) {
                            imageViewCamera.setVisibility(View.VISIBLE);
                            imageViewCamera.animate().scaleX(1f).scaleY(1f).setDuration(100).setInterpolator(new LinearInterpolator()).start();
                        }
                    }

                } else {
                    if (imageViewSend.getVisibility() != View.VISIBLE && !isLocked) {
                        imageViewSend.setVisibility(View.VISIBLE);
                        imageViewSend.animate().scaleX(1f).scaleY(1f).setDuration(100).setInterpolator(new LinearInterpolator()).start();
                    }

                    if (showCameraIcon) {
                        if (imageViewCamera.getVisibility() != View.INVISIBLE) {
                            imageViewCamera.setVisibility(View.INVISIBLE);
                            imageViewCamera.animate().scaleX(0f).scaleY(0f).setDuration(100).setInterpolator(new LinearInterpolator()).start();
                        }
                    }
                }
            }
        });
//
//        imageViewAudio.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                try {
//                    // Create folder to store recordingss
//                    File myDirectory = new File(Environment.getExternalStorageDirectory(), "11zon");
//                    if (!myDirectory.exists()) {
//                        myDirectory.mkdirs();
//                    }
//                    SimpleDateFormat dateFormat = new SimpleDateFormat("mmddyyyyhhmmss");
//                    String date = dateFormat.format(new Date());
//                    audioFile = "REC" + date;
//                    filePath = myDirectory.getAbsolutePath() + File.separator + audioFile;
//                    startAudioRecorder();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                timeText.setVisibility(View.VISIBLE);
//                imageViewMic.setVisibility(View.VISIBLE);
//                timeText.startAnimation(animBlink);
//                showTimer();
//                return false;
//            }
//        });


//        imageViewAudio.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//
//
//                    if (isDeleting) {
//                        return true;
//                    }
//                long duration = motionEvent.getEventTime() - motionEvent.getDownTime();
//
//
//                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
//
//                    cancelOffset = (float) (screenWidth / 2.8);
//                    lockOffset = (float) (screenWidth / 2.5);
//
//                    if (firstX == 0) {
//                        firstX = motionEvent.getRawX();
//                    }
//
//                    if (firstY == 0) {
//                        firstY = motionEvent.getRawY();
//                    }
//
//
//
//                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP
//                        || motionEvent.getAction() == MotionEvent.ACTION_CANCEL) {
//
//                    if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
//                        stopRecording(RecordingBehaviour.RELEASED);
//                    }
//
//                } else if (motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
//
//                    if (stopTrackingAction) {
//                        return true;
//                    }
//
//                    UserBehaviour direction = UserBehaviour.NONE;
//
//                    float motionX = Math.abs(firstX - motionEvent.getRawX());
//                    float motionY = Math.abs(firstY - motionEvent.getRawY());
//
//                    if (isLayoutDirectionRightToLeft ? (motionX > directionOffset && lastX > firstX && lastY > firstY) : (motionX > directionOffset && lastX < firstX && lastY < firstY)) {
//
//                        if (isLayoutDirectionRightToLeft ? (motionX > motionY && lastX > firstX) : (motionX > motionY && lastX < firstX)) {
//                            direction = UserBehaviour.CANCELING;
//
//                        } else if (motionY > motionX && lastY < firstY) {
//                            direction = UserBehaviour.LOCKING;
//                        }
//
//                    } else if (isLayoutDirectionRightToLeft ? (motionX > motionY && motionX > directionOffset && lastX > firstX) : (motionX > motionY && motionX > directionOffset && lastX < firstX)) {
//                        direction = UserBehaviour.CANCELING;
//                    } else if (motionY > motionX && motionY > directionOffset && lastY < firstY) {
//                        direction = UserBehaviour.LOCKING;
//                    }
//
//                    if (direction == UserBehaviour.CANCELING) {
//                        if (userBehaviour == UserBehaviour.NONE || motionEvent.getRawY() + imageViewAudio.getWidth() / 2 > firstY) {
//                            userBehaviour = UserBehaviour.CANCELING;
//                        }
//
//                        if (userBehaviour == UserBehaviour.CANCELING) {
//                            translateX(-(firstX - motionEvent.getRawX()));
//                        }
//                    } else if (direction == UserBehaviour.LOCKING) {
//                        if (userBehaviour == UserBehaviour.NONE || motionEvent.getRawX() + imageViewAudio.getWidth() / 2 > firstX) {
//                            userBehaviour = UserBehaviour.LOCKING;
//                        }
//
//                        if (userBehaviour == UserBehaviour.LOCKING) {
//                            translateY(-(firstY - motionEvent.getRawY()));
//                        }
//                    }
//
//                    lastX = motionEvent.getRawX();
//                    lastY = motionEvent.getRawY();
//                }
//                view.onTouchEvent(motionEvent);
//                return true;
//            }
//        });

        imageViewStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isLocked = false;
                stopRecording(RecordingBehaviour.LOCK_DONE);
            }
        });
    }

    private void translateY(float y) {
        if (y < -lockOffset) {
            locked();
            imageViewAudio.setTranslationY(0);
            return;
        }

//        if (layoutLock.getVisibility() != View.VISIBLE) {
//            layoutLock.setVisibility(View.VISIBLE);
//        }

        imageViewAudio.setTranslationY(y);
        //layoutLock.setTranslationY(y / 2);
        imageViewAudio.setTranslationX(0);
    }

    private void translateX(float x) {

        if (isLayoutDirectionRightToLeft ? x > cancelOffset : x < -cancelOffset) {
            canceled();
            imageViewAudio.setTranslationX(0);
            layoutSlideCancel.setTranslationX(0);
            return;
        }

        imageViewAudio.setTranslationX(x);
        layoutSlideCancel.setTranslationX(x);
        // layoutLock.setTranslationY(0);
        imageViewAudio.setTranslationY(0);

//        if (Math.abs(x) < imageViewMic.getWidth() / 2) {
//            if (layoutLock.getVisibility() != View.VISIBLE) {
//                layoutLock.setVisibility(View.VISIBLE);
//            }
//        } else {
//            if (layoutLock.getVisibility() != View.GONE) {
//                layoutLock.setVisibility(View.GONE);
//            }
//        }
    }

    public void startAudioRecorder() {
        try {
            mediaRecorder = new MediaRecorder();
            mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            mediaRecorder.setOutputFile(filePath);
            mediaRecorder.prepare();
            mediaRecorder.start();
        }catch (Exception  e) {
            e.printStackTrace();
        }
    }
    private void locked() {
        stopTrackingAction = true;
        stopRecording(RecordingBehaviour.LOCKED);
        isLocked = true;
    }

    private void canceled() {
        stopTrackingAction = true;
        stopRecording(RecordingBehaviour.CANCELED);
    }

    private void stopRecording(RecordingBehaviour recordingBehaviour) {
        if (recordingBehaviour == null) {
            return;
        }

        stopRecording();

        stopTrackingAction = true;
        firstX = 0;
        firstY = 0;
        lastX = 0;
        lastY = 0;

        userBehaviour = UserBehaviour.NONE;

        imageViewAudio.animate().scaleX(1f).scaleY(1f).translationX(0).translationY(0).setDuration(100).setInterpolator(new LinearInterpolator()).start();
        layoutSlideCancel.setTranslationX(0);
        layoutSlideCancel.setVisibility(View.GONE);

        // layoutLock.setVisibility(View.GONE);
        // layoutLock.setTranslationY(0);
        imageViewLockArrow.clearAnimation();
        imageViewLock.clearAnimation();

        if (isLocked) {
            return;
        }

        if (recordingBehaviour == RecordingBehaviour.LOCKED) {
            imageViewStop.setVisibility(View.VISIBLE);

            if (recordingListener != null)
                recordingListener.onRecordingLocked();

        } else if (recordingBehaviour == RecordingBehaviour.CANCELED) {
            timeText.clearAnimation();
            timeText.setVisibility(View.INVISIBLE);
            imageViewMic.setVisibility(View.INVISIBLE);
            imageViewStop.setVisibility(View.GONE);
            layoutEffect2.setVisibility(View.GONE);
            layoutEffect1.setVisibility(View.GONE);

            timerTask.cancel();
            delete();

            if (recordingListener != null)
                recordingListener.onRecordingCanceled();

        } else if (recordingBehaviour == RecordingBehaviour.RELEASED || recordingBehaviour == RecordingBehaviour.LOCK_DONE) {
            timeText.clearAnimation();
            timeText.setVisibility(View.INVISIBLE);
            imageViewMic.setVisibility(View.INVISIBLE);
            editTextMessage.setVisibility(View.VISIBLE);
            if (showAttachmentIcon) {
            }
            if (showCameraIcon) {
                imageViewCamera.setVisibility(View.VISIBLE);
            }
            if (showEmojiIcon) {
            }
            imageViewStop.setVisibility(View.GONE);
            editTextMessage.requestFocus();
            layoutEffect2.setVisibility(View.GONE);
            layoutEffect1.setVisibility(View.GONE);

            timerTask.cancel();

            if (recordingListener != null) {

            }
            //System.out.println("on complete file Name"+fileName);
            // recordingListener.onRecordingCompleted(fileName);
        }
    }

    private void startRecord() {

        if (recordingListener != null)
            recordingListener.onRecordingStarted();
        startRecording();

        hideAttachmentOptionView();

        stopTrackingAction = false;
        editTextMessage.setVisibility(View.INVISIBLE);

        imageViewCamera.setVisibility(View.INVISIBLE);

        imageViewAudio.animate().scaleXBy(1f).scaleYBy(1f).setDuration(200).setInterpolator(new OvershootInterpolator()).start();
        timeText.setVisibility(View.VISIBLE);
        //  layoutLock.setVisibility(View.VISIBLE);
        layoutSlideCancel.setVisibility(View.VISIBLE);
        imageViewMic.setVisibility(View.VISIBLE);
        layoutEffect2.setVisibility(View.VISIBLE);
        layoutEffect1.setVisibility(View.VISIBLE);

        timeText.startAnimation(animBlink);
        imageViewLockArrow.clearAnimation();
        imageViewLock.clearAnimation();
        imageViewLockArrow.startAnimation(animJumpFast);
        imageViewLock.startAnimation(animJump);

        if (audioTimer == null) {
            audioTimer = new Timer();
            timeFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        }

        timerTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        timeText.setText(timeFormatter.format(new Date(audioTotalTime * 1000)));
                        audioTotalTime++;
                    }
                });
            }
        };

        audioTotalTime = 0;
        audioTimer.schedule(timerTask, 0, 1000);
    }
    public void showTimer() {
        countDownTimer = new CountDownTimer(Long.MAX_VALUE, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                second++;
                timeText.setText(recorderTime());
            }
            public void onFinish() {

            }
        };
        countDownTimer.start();
    }

    //recorder time
    public String recorderTime() {
        if (second == 60) {
            minute++;
            second = 0;
        }
        if (minute == 60) {
            hour++;
            minute = 0;
        }
        return String.format("%02d:%02d:%02d", hour, minute, second);
    }
    private void delete() {
        imageViewMic.setVisibility(View.VISIBLE);
        imageViewMic.setRotation(0);
        isDeleting = true;
        imageViewAudio.setEnabled(false);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                isDeleting = false;
                imageViewAudio.setEnabled(true);

                if (showAttachmentIcon) {
                }
                if (showCameraIcon) {
                    imageViewCamera.setVisibility(View.VISIBLE);
                }
                if (showEmojiIcon) {
                }
            }
        }, 1250);

        imageViewMic.animate().translationY(-dp * 150).rotation(180).scaleXBy(0.6f).scaleYBy(0.6f).setDuration(500).setInterpolator(new DecelerateInterpolator()).setListener(new Animator.AnimatorListener() {

            @Override
            public void onAnimationStart(Animator animation) {

                float displacement = 0;

                if (isLayoutDirectionRightToLeft) {
                    displacement = dp * 40;
                } else {
                    displacement = -dp * 40;
                }

                dustin.setTranslationX(displacement);
                dustin_cover.setTranslationX(displacement);

                dustin_cover.animate().translationX(0).rotation(-120).setDuration(350).setInterpolator(new DecelerateInterpolator()).start();

                dustin.animate().translationX(0).setDuration(350).setInterpolator(new DecelerateInterpolator()).setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        dustin.setVisibility(View.VISIBLE);
                        dustin_cover.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {

                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                }).start();
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                imageViewMic.animate().translationY(0).scaleX(1).scaleY(1).setDuration(350).setInterpolator(new LinearInterpolator()).setListener(
                        new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                imageViewMic.setVisibility(View.INVISIBLE);
                                imageViewMic.setRotation(0);

                                float displacement = 0;

                                if (isLayoutDirectionRightToLeft) {
                                    displacement = dp * 40;
                                } else {
                                    displacement = -dp * 40;
                                }

                                dustin_cover.animate().rotation(0).setDuration(150).setStartDelay(50).start();
                                dustin.animate().translationX(displacement).setDuration(200).setStartDelay(250).setInterpolator(new DecelerateInterpolator()).start();
                                dustin_cover.animate().translationX(displacement).setDuration(200).setStartDelay(250).setInterpolator(new DecelerateInterpolator()).setListener(new Animator.AnimatorListener() {
                                    @Override
                                    public void onAnimationStart(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        editTextMessage.setVisibility(View.VISIBLE);
                                        editTextMessage.requestFocus();
                                    }

                                    @Override
                                    public void onAnimationCancel(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationRepeat(Animator animation) {

                                    }
                                }).start();
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        }
                ).start();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        }).start();
    }

    private void showErrorLog(String s) {
        Log.e(TAG, s);
    }


    private void startRecording() {
//        recorder = new MediaRecorder();
//        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
//        recorder.setOutputFormat(output_formats[currentFormat]);
//        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
//        recorder.setOutputFile(getFilename());
//        fileName = getFilename();
//        System.out.println("fil name" + getFilename());
//        recorder.setOnErrorListener(errorListener);
//        recorder.setOnInfoListener(infoListener);
//
//        try {
//            recorder.prepare();
//            recorder.start();
//        } catch (IllegalStateException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        AudioSavePathInDevice =
                Environment.getExternalStorageDirectory().getAbsolutePath() + "/" +
                        CreateRandomAudioFileName(5) + "AudioRecording.3gp";

        MediaRecorderReady();

        try {
            mediaRecorder.prepare();
            mediaRecorder.start();
        } catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        Toast.makeText(context, "Recording started",
                Toast.LENGTH_LONG).show();
    }

    public void MediaRecorderReady(){
        mediaRecorder=new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mediaRecorder.setOutputFile(AudioSavePathInDevice);
    }

    public String CreateRandomAudioFileName(int string){
        StringBuilder stringBuilder = new StringBuilder( string );
        int i = 0 ;
        while(i < string ) {
            stringBuilder.append(RandomAudioFileName.
                    charAt(random.nextInt(RandomAudioFileName.length())));

            i++ ;
        }
        return stringBuilder.toString();
    }

    private MediaRecorder.OnErrorListener errorListener = new MediaRecorder.OnErrorListener() {
        @Override
        public void onError(MediaRecorder mr, int what, int extra) {

            Toast.makeText(context, "" + what, Toast.LENGTH_SHORT).show();
        }
    };

    private MediaRecorder.OnInfoListener infoListener = new MediaRecorder.OnInfoListener() {
        @Override
        public void onInfo(MediaRecorder mr, int what, int extra) {
            System.out.println("file  " + extra);
            Toast.makeText(context, "" + what, Toast.LENGTH_SHORT).show();
        }
    };

    private String getFilename(){
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath,AUDIO_RECORDER_FOLDER);

        if(!file.exists()){
            file.mkdirs();
        }

        return (file.getAbsolutePath() + "/" + System.currentTimeMillis() + file_exts[currentFormat]);
    }

    private void stopRecording() {
        System.out.println("on complete file Name" + fileName);
        recordingListener.onRecordingCompleted(AudioSavePathInDevice);
        if(mediaRecorder!=null)

        try {
            mediaRecorder.stop();
            mediaRecorder.release();

        }
        catch (IllegalStateException ex){

        }
        Toast.makeText(context, "Recording Completed",
                Toast.LENGTH_LONG).show();
    }
}
