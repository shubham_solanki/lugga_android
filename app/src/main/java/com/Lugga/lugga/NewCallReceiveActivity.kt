package com.Lugga.lugga

import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Message
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.Lugga.lugga.utils.SocketNetworking
import com.facebook.react.modules.core.PermissionListener
import io.socket.client.Socket
import kotlinx.android.synthetic.main.activity_new_call_receive.*
import org.jitsi.meet.sdk.*
import org.json.JSONObject

class NewCallReceiveActivity : AppCompatActivity(), View.OnClickListener, JitsiMeetActivityInterface {
    private var options: JitsiMeetConferenceOptions?=null
    private var jitsiMeerView: JitsiMeetView? = null
    private var socket: Socket? = null
    private var mMediaPlayer: MediaPlayer? = null
    var callerId: String? = null
    var receiverId: String? = null
    var callerName: String? = null
    var userType: String? = null
    var callType: String? = null
    var senderName: String? = null
    var bookingId: String? = null
    var channelId: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_call_receive)
      //  setRingTone()
        connectSocket()
        getData()
        checkSocketForCallCut()
        ivNewAcceptCall.setOnClickListener(this)
        ivNewRejectCall.setOnClickListener(this)
        var animShake= AnimationUtils.loadAnimation(this,R.anim.shake)
        ivNewAcceptCall.animation=animShake

    }
    override fun onStop() {
        JitsiMeetActivityDelegate.onHostPause(this);
        super.onStop()
    }
    override fun onResume() {
        JitsiMeetActivityDelegate.onHostResume(this);
        super.onResume()
    }
    override fun onBackPressed() {
        JitsiMeetActivityDelegate.onNewIntent(intent);
        super.onBackPressed()
    }

    override fun onDestroy() {
        jitsiMeerView?.dispose();
        jitsiMeerView = null;

        JitsiMeetActivityDelegate.onHostDestroy(this);
        super.onDestroy()
    }
    private fun checkSocketForCallCut() {
        if (socket!!.connected()) {
            socket!!.on("returncallStatusRequest" + bookingId) { args ->
                runOnUiThread(Runnable {
                    val msg = Message()
                    val jobj = args[0] as JSONObject
                    msg.obj = jobj
                    if (jobj.getString("status") != null) {
                        if (jobj.getString("status").equals("1")) {
                            getReturnAcceptResponseOfCall()
                        } else {
                            if (jitsiMeerView != null) {
                                jitsiMeerView!!.leave()
                                jitsiMeerView!!.dispose()
                            }
                            finish()
                        }
                    }
                })


            }
        } else {
            socket!!.connect()
            socket!!.on("returncallStatusRequest" + bookingId) { args ->
                runOnUiThread(Runnable {
                    val msg = Message()
                    val jobj = args[0] as JSONObject
                    msg.obj = jobj
                    if (jobj.getString("status") != null) {
                        if (jobj.getString("status").equals("1")) {
                            getReturnAcceptResponseOfCall()
                        } else {
                            hitSocketForCallTerminate()
                            if (jitsiMeerView != null) {
                                jitsiMeerView!!.leave()
                                jitsiMeerView!!.dispose()
                            }
                            finish()
                        }
                    }
                })
            }
        }
    }

    private fun getData() {
        val intent = intent ?: return
        if (intent.getStringExtra("channel_id") != null) {
            channelId = intent.getStringExtra("channel_id")
        //    Log.e("channedCallRecId",channelId)
            callerId = intent.getStringExtra("caller_id")
            receiverId = intent.getStringExtra("receiver_id")
            callerName = intent.getStringExtra("caller_name")
            userType = intent.getStringExtra("user_type")
            callType = intent.getStringExtra("call_type")
            senderName = intent.getStringExtra("sender_name")
            bookingId = intent.getStringExtra("booking_id")
        }

        setData()

    }

    private fun setData() {
        if (callType.equals("1")) {
            tvNewCallType.text = getString(R.string.audiocall)
        } else {
            tvNewCallType.text = getString(R.string.videocall)
        }
        tvNewCallerName.text=callerName
    }

    private fun setRingTone() {
        mMediaPlayer = MediaPlayer.create(this, R.raw.incoming_tone)
        mMediaPlayer?.setAudioStreamType(AudioManager.STREAM_MUSIC)
        mMediaPlayer?.setLooping(true)
        mMediaPlayer?.start()
    }

    private fun connectSocket() {
        try {
            socket = SocketNetworking.getSocket()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            ivNewRejectCall -> {
                if (mMediaPlayer != null) {
                    mMediaPlayer?.stop()
                }
                emitForCallCutResponse("3")
            }
            ivNewAcceptCall -> {
                if (mMediaPlayer != null) {
                    mMediaPlayer?.stop()
                }
                emitForCallAcceptResponse("1")
            }
        }

    }

    private fun emitForCallAcceptResponse(status: String) {
        if (socket!!.connected()) {
            val obj: JSONObject = JSONObject()
            obj.put("user_type", userType)
            obj.put("call_type", callType)
            obj.put("caller_id", callerId)
            obj.put("receiever_id", receiverId)
            obj.put("booking_id", bookingId)
            obj.put("time_count", "0")
            obj.put("status", status)
            socket!!.emit("callStatusRequest", obj)

        } else {
            socket!!.connect()
        }
    }

    private fun getReturnAcceptResponseOfCall() {
        if(callType.equals("1")) {
             options = JitsiMeetConferenceOptions.Builder()
                    .setRoom(channelId)
                    .setAudioOnly(true)
                    .setFeatureFlag("call-integration.enabled", false)
                    .build()
        }
        else{
             options = JitsiMeetConferenceOptions.Builder()
                    .setRoom(channelId)
                    .setAudioOnly(false)
                    .setFeatureFlag("call-integration.enabled", false)
                    .build() 
        }

        jitsiMeerView = JitsiMeetView(this)
        jitsiMeerView?.listener = object : JitsiMeetViewListener {

            override fun onConferenceTerminated(p0: MutableMap<String, Any>?) {
                hitSocketForCallTerminate()
                Log.e("callReceive","termminated")
            }

            override fun onConferenceJoined(p0: MutableMap<String, Any>?) {
                Log.e("callReceive","joined")
                

            }

            override fun onConferenceWillJoin(p0: MutableMap<String, Any>?) {
                Log.e("callReceive","willjoin")

            }

        }
        jitsiMeerView!!.join(options)
        setContentView(jitsiMeerView)
    }

    private fun hitSocketForCallTerminate() {
        if (socket!!.connected()) {
            val obj: JSONObject = JSONObject()
            obj.put("time_count", "0")
            obj.put("status", "2")
            obj.put("endCall", "receiver")
            obj.put("booking_id", bookingId)
            obj.put("receiever_id", receiverId)
            obj.put("channel_id", channelId)
            obj.put("caller_id", callerId)
            obj.put("user_type", userType)
            obj.put("call_type", callType)
            socket!!.emit("callStatusRequest", obj)

        } else {
            socket!!.connect()
            Toast.makeText(this, "something went wrong !! please try again", Toast.LENGTH_SHORT).show()
        }
    }


    private fun emitForCallCutResponse(status: String) {
        if (socket!!.connected()) {
            val obj: JSONObject = JSONObject()
            obj.put("user_type", userType)
            obj.put("call_type", callType)
            obj.put("caller_id", callerId)
            obj.put("receiever_id", receiverId)
            obj.put("booking_id", bookingId)
            obj.put("time_count", "0")
            obj.put("status", status)
            obj.put("endCall", "receiver")
            socket!!.emit("callStatusRequest", obj)
        } else {
            socket!!.connect()
        }

    }


    override fun requestPermissions(p0: Array<out String>?, p1: Int, p2: PermissionListener?) {

    }
}