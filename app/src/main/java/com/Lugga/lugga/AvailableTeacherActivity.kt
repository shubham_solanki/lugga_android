package com.Lugga.lugga

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.Lugga.lugga.adapters.AdapterAvailableTeacher
import com.Lugga.lugga.adapters.AdapterAvailableTeacher.AvailableTeacherView
import com.Lugga.lugga.model.searchteacher.SearchTeacherBodyItem
import io.socket.client.Socket
import kotlinx.android.synthetic.main.available_teacher.*
import java.util.*

class AvailableTeacherActivity : AppCompatActivity(), AvailableTeacherView {
    var recyclerView: RecyclerView? = null
    var adapterAvailableTeacher: AdapterAvailableTeacher? = null
    var socket: Socket? = null
    var searchTeacherBodyItemArrayList: ArrayList<SearchTeacherBodyItem>? = null
    var noteacherAvaliable: TextView? = null
    var ivBack: ImageView? = null
    private var bookingId = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.available_teacher)
        ivBack = findViewById(R.id.ivBack)
        intent = getIntent()
        bookingId = intent!!.getIntExtra("bookingId", 0)
        searchTeacherBodyItemArrayList = intent!!.getSerializableExtra("teacherlist") as ArrayList<SearchTeacherBodyItem>
        recyclerView = findViewById(R.id.recyclerAvailbaleTeacher)
        noteacherAvaliable = findViewById(R.id.tvNoTeacherAvaliable)
        setAdapter()
        ivBack!!.setOnClickListener(View.OnClickListener { onBackPressed() })
        searchTeacher()
    }

    private fun searchTeacher() {
        etSearch.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                adapterAvailableTeacher?.getFilter()?.filter(s.toString())
            }

        })
    }

    private fun setAdapter() {
        if (searchTeacherBodyItemArrayList!!.size > 0) {
            adapterAvailableTeacher = AdapterAvailableTeacher(this@AvailableTeacherActivity, searchTeacherBodyItemArrayList!!, this)
            recyclerView!!.adapter = adapterAvailableTeacher
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        finish()
    }

    override fun onClickTeacherSelect(id: Int?) {
        val i = Intent(this, TeacherDetailsBookingActivity::class.java)
        i.putExtra("teacherId", id)
        i.putExtra("bookingID", bookingId)
        startActivity(i)
    }

}