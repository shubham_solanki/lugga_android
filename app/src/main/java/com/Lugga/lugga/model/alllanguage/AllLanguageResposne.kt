package com.Lugga.lugga.model.alllanguage

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class AllLanguageResposne {
    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("body")
    @Expose
    var body: ArrayList<AllLanguagModel>? = null

    @SerializedName("success")
    @Expose
    var success: Int? = null

    @SerializedName("currentdateTime")
    @Expose
    var currentdateTime: String? = null


    var popular: ArrayList<AllLanguagModel>? = null
    var other: ArrayList<AllLanguagModel>? = null


}