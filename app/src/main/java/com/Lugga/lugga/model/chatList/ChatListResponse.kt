package com.Lugga.lugga.model.chatList

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class ChatListResponse {
    @SerializedName("success")
    @Expose
    var success: Int? = null

    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("body")
    @Expose
    var body: ArrayList<ChatListBodyItem>? = null

}