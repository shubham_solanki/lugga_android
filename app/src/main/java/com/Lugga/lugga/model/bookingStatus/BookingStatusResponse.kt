package com.Lugga.lugga.model.bookingStatus

import com.Lugga.lugga.model.BodyItem
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class BookingStatusResponse {
    var id = 0

    @JvmField
    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("body")
    @Expose
    var body: List<BodyItem>? = null

    @JvmField
    @SerializedName("success")
    @Expose
    var success: Int? = null

}