package com.Lugga.lugga.model.createbooking

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CreateBookingBodyItem {
    @SerializedName("user_id")
    @Expose
    var userId: Int? = null

    @SerializedName("amount")
    @Expose
    var amount: Int? = null

    @SerializedName("time")
    @Expose
    var time: String? = null

    @SerializedName("name")
    @Expose
    var name: String? = null

}