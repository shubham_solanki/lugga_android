package com.Lugga.lugga.model.getTeacherDetail

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class TeacherDetailResponse {
    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("body")
    @Expose
    var body: ArrayList<TeacherDetailBodyItem>? = null

    @SerializedName("success")
    @Expose
    var success: Int? = null

    @SerializedName("feedback")
    @Expose
    var feedback: ArrayList<Any>? = null

}