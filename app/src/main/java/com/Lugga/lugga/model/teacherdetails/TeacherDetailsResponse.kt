package com.Lugga.lugga.model.teacherdetails

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class TeacherDetailsResponse {
    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("body")
    @Expose
    var body: ArrayList<TeacherDetailsBodyItem>? = null

    @SerializedName("success")
    @Expose
    var success: Int? = null

    @SerializedName("feedback")
    @Expose
    var feedback: ArrayList<TeacherDetailsBodyItem>? = null

}