package com.Lugga.lugga.model.teacherprofile

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class TeacherProfileResponse {
    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("body")
    @Expose
    var body: ArrayList<TeacherProfileBodyItem>? = null

    @SerializedName("success")
    @Expose
    var success: Int? = null

}