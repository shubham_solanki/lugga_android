package com.Lugga.lugga.model.createbooking

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class CreateBookingResponse {
    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("body")
    @Expose
    var body: ArrayList<CreateBookingBodyItem>? = null

    @SerializedName("success")
    @Expose
    var success: Int? = null

}