package com.Lugga.lugga.model.sendRequest

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SendRequestBodyItem {
    @SerializedName("id")
    @Expose
    var id: Int? = null

    @SerializedName("learner_id")
    @Expose
    var learnerId: Int? = null

    @SerializedName("teacher_id")
    @Expose
    var teacherId: Int? = null

    @SerializedName("booking_type")
    @Expose
    var bookingType: Int? = null

    @SerializedName("session_time")
    @Expose
    var sessionTime: String? = null

    @SerializedName("language")
    @Expose
    var language: String? = null

    @SerializedName("start_session")
    @Expose
    var startSession: String? = null

    @SerializedName("end_session")
    @Expose
    var endSession: String? = null

    @SerializedName("rating")
    @Expose
    var rating: String? = null

    @SerializedName("comment")
    @Expose
    var comment: Any? = null

    @SerializedName("payment_id")
    @Expose
    var paymentId: String? = null

    @SerializedName("payment_amount")
    @Expose
    var paymentAmount: Int? = null

    @SerializedName("level")
    @Expose
    var level: String? = null

    @SerializedName("status")
    @Expose
    var status: Int? = null

    @SerializedName("created_at")
    @Expose
    var createdAt: String? = null

}