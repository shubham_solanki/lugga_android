package com.Lugga.lugga.model.searchteacher

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class SearchTeacherResponse {
    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("body")
    @Expose
    var body: ArrayList<SearchTeacherBodyItem>? = null

    @SerializedName("success")
    @Expose
    var success: Int? = null

    @SerializedName("bookingID")
    @Expose
    var bookingID: Int? = null

}