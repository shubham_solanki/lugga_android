package com.Lugga.lugga.model.learnerhistory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class LearnerHistoryBodyItem {
	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("learner_id")
	@Expose
	private Integer learnerId;
	@SerializedName("teacher_id")
	@Expose
	private Integer teacherId;
	@SerializedName("booking_type")
	@Expose
	private Integer bookingType;
	@SerializedName("session_time")
	@Expose
	private String sessionTime;
	@SerializedName("language")
	@Expose
	private String language;
	@SerializedName("start_session")
	@Expose
	private String startSession;
	@SerializedName("end_session")
	@Expose
	private String endSession;
	@SerializedName("rating")
	@Expose
	private String rating;
	@SerializedName("comment")
	@Expose
	private String comment;
	@SerializedName("payment_id")
	@Expose
	private String paymentId;
	@SerializedName("payment_amount")
	@Expose
	private Integer paymentAmount;
	@SerializedName("level")
	@Expose
	private String level;
	@SerializedName("status")
	@Expose
	private String status;
	@SerializedName("created_at")
	@Expose
	private Date createdAt;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("email")
	@Expose
	private String email;
	@SerializedName("mobile_no")
	@Expose
	private String mobileNo;
	@SerializedName("password")
	@Expose
	private String password;
	@SerializedName("profile_image")
	@Expose
	private String profileImage;
	@SerializedName("user_type")
	@Expose
	private Integer userType;
	@SerializedName("device_type")
	@Expose
	private String deviceType;
	@SerializedName("device_token")
	@Expose
	private String deviceToken;
	@SerializedName("push_kit_token")
	@Expose
	private String pushKitToken;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getLearnerId() {
		return learnerId;
	}

	public void setLearnerId(Integer learnerId) {
		this.learnerId = learnerId;
	}

	public Integer getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Integer teacherId) {
		this.teacherId = teacherId;
	}

	public Integer getBookingType() {
		return bookingType;
	}

	public void setBookingType(Integer bookingType) {
		this.bookingType = bookingType;
	}

	public String getSessionTime() {
		return sessionTime;
	}

	public void setSessionTime(String sessionTime) {
		this.sessionTime = sessionTime;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getStartSession() {
		return startSession;
	}

	public void setStartSession(String startSession) {
		this.startSession = startSession;
	}

	public String getEndSession() {
		return endSession;
	}

	public void setEndSession(String endSession) {
		this.endSession = endSession;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public Integer getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(Integer paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}

	public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}

	public String getPushKitToken() {
		return pushKitToken;
	}

	public void setPushKitToken(String pushKitToken) {
		this.pushKitToken = pushKitToken;
	}
}
