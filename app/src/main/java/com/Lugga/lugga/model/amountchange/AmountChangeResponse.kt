package com.Lugga.lugga.model.amountchange

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AmountChangeResponse {
    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("body")
    @Expose
    var body: List<Any>? = null

    @SerializedName("success")
    @Expose
    var success: Int? = null

}