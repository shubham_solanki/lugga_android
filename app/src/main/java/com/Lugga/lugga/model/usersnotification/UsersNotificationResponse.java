package com.Lugga.lugga.model.usersnotification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class UsersNotificationResponse{
	@SerializedName("message")
	@Expose
	private String message;
	@SerializedName("body")
	@Expose
	private ArrayList<UsersNotificationBodyItem> body = null;
	@SerializedName("success")
	@Expose
	private Integer success;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ArrayList<UsersNotificationBodyItem> getBody() {
		return body;
	}

	public void setBody(ArrayList<UsersNotificationBodyItem> body) {
		this.body = body;
	}

	public Integer getSuccess() {
		return success;
	}

	public void setSuccess(Integer success) {
		this.success = success;
	}
}