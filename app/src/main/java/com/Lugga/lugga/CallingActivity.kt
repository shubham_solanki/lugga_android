package com.Lugga.lugga

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Message
import android.util.Log
import android.view.View
import android.widget.Toast
import com.Lugga.lugga.utils.SocketNetworking
import com.facebook.react.modules.core.PermissionListener
import io.socket.client.Socket
import kotlinx.android.synthetic.main.activity_calling.*
import org.jitsi.meet.sdk.*
import org.json.JSONObject

class CallingActivity : AppCompatActivity(), JitsiMeetActivityInterface {
    private var options: JitsiMeetConferenceOptions? = null
    private var isStart: Boolean? = false
    private var socket: Socket? = null
    private var channelId: String? = null
    private var receiverId: String? = null
    private var bookingId: String? = null
    private var callerId: String? = null
    private var callType: String? = null
    private var userType: String? = null
    private var jitsiMeerView: JitsiMeetView? = null
    var countDownTimer: CountDownTimer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calling)
        connectSocket()
        channelId = intent.getStringExtra("channelId");

        receiverId = intent.getStringExtra("receiverId")
        bookingId = intent.getStringExtra("bookingId");
        callerId = intent.getStringExtra("callerId");
        userType = intent.getStringExtra("userType");
        callType = intent.getStringExtra("callType");
        //Log.e("callType",callType)


        socketCutCallResponse()
        countDownTimer = object : CountDownTimer(1000, 1000) {
            override fun onFinish() {
                setForCall()

            }

            override fun onTick(millisUntilFinished: Long) {

            }

        }
        countDownTimer?.start()


    }

    override fun onStop() {
        JitsiMeetActivityDelegate.onHostPause(this);
        super.onStop()
    }

    override fun onResume() {
        JitsiMeetActivityDelegate.onHostResume(this);
        super.onResume()
    }

    override fun onBackPressed() {
        JitsiMeetActivityDelegate.onNewIntent(intent);
        super.onBackPressed()
    }

    private fun connectSocket() {
        try {
            socket = SocketNetworking.getSocket()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        socket!!.connect()
    }

    override fun onDestroy() {
        jitsiMeerView?.dispose();
        jitsiMeerView = null;

        JitsiMeetActivityDelegate.onHostDestroy(this);
        super.onDestroy()
    }

    private fun setForCall() {
       // Log.e("calltype", callType)
        if (callType.equals("1")) {                                   //audio call
            options = JitsiMeetConferenceOptions.Builder()
                    .setRoom(channelId)
                    .setAudioOnly(true)
                    .setFeatureFlag("call-integration.enabled", false)
                    .build()
        } else {
            options = JitsiMeetConferenceOptions.Builder()                 //video call
                    .setRoom(channelId)
                    .setAudioOnly(false)
                    .setFeatureFlag("call-integration.enabled", false)
                    .build()
        }

        jitsiMeerView = JitsiMeetView(this)
        jitsiMeerView?.listener = object : JitsiMeetViewListener {
            override fun onConferenceTerminated(p0: MutableMap<String, Any>?) {
                hitSocketForCallTerminate()
                Log.e("callStart", "termminated")
            }

            override fun onConferenceJoined(p0: MutableMap<String, Any>?) {
                Log.e("callStart", "joined")

            }

            override fun onConferenceWillJoin(p0: MutableMap<String, Any>?) {
                Log.e("callStart", "willJoin")

            }

        }
        progressBar.visibility = View.GONE
        jitsiMeerView?.join(options)
        setContentView(jitsiMeerView)

    }

    private fun hitSocketForCallTerminate() {
        if (socket!!.connected()) {
            val obj: JSONObject = JSONObject()
            obj.put("time_count", "0")
            obj.put("status", "2")
            obj.put("endCall", "sender")
            obj.put("booking_id", bookingId)
            obj.put("receiever_id", receiverId)
            obj.put("channel_id", channelId)
            obj.put("caller_id", callerId)
            obj.put("user_type", userType)
            obj.put("call_type", callType)
            socket!!.emit("callStatusRequest", obj)

        } else {
            socket!!.connect()
            Toast.makeText(this, "something went wrong !! please try again", Toast.LENGTH_SHORT).show()
        }
    }

    private fun socketCutCallResponse() {
        if (socket!!.connected()) {
            socket!!.on("returncallStatusRequest" + bookingId) { args ->
                runOnUiThread(Runnable {
                    val msg = Message()
                    val jobj = args[0] as JSONObject
                    msg.obj = jobj
                    if (jobj.getString("status") != null) {
                        if (jobj.getString("status").equals("1")) {
                            Log.e("yescallInagain", "true")
                        } else {
                            if (jitsiMeerView != null) {
                                jitsiMeerView?.leave()
                                jitsiMeerView?.dispose()
                            }
                            finish()
                        }
                    }
                })
            }

        } else {
            socket!!.connect()
            socket!!.on("returncallStatusRequest" + bookingId) { args ->
                runOnUiThread(Runnable {
                    val msg = Message()
                    val jobj = args[0] as JSONObject
                    msg.obj = jobj
                    if (jobj.getString("status") != null) {
                        if (jobj.getString("status").equals("1")) {
                        } else {
                            if (jitsiMeerView != null) {
                                jitsiMeerView?.leave()
                                jitsiMeerView?.dispose()
                            }
                            finish()
                        }
                    }
                })
            }
        }
    }


    override fun requestPermissions(p0: Array<out String>?, p1: Int, p2: PermissionListener?) {

    }
}