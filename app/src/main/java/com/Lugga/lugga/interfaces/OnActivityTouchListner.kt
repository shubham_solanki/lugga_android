package com.Lugga.lugga.interfaces

import android.view.MotionEvent

interface OnActivityTouchListner {
    fun getTouchCoordinates(ev: MotionEvent?)
}