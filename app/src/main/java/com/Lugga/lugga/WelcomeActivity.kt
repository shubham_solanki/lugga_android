package com.Lugga.lugga

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.Lugga.lugga.sharedpreferences.UsersharedPrefernce
import kotlinx.android.synthetic.main.activity_welcome.*

class WelcomeActivity : AppCompatActivity() {
    var usersharedPrefernce: UsersharedPrefernce? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)
        tvContinue.setOnClickListener {
            usersharedPrefernce = UsersharedPrefernce.getInstance()
            if (usersharedPrefernce!!.getboolean()) {
                val intent = Intent(this, HomeActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
    }
}