package com.Lugga.lugga

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.Lugga.lugga.adapters.IntroSliderAdapter
import com.Lugga.lugga.fragments.IntroFirstFragment
import com.Lugga.lugga.fragments.IntroSecondFragment
import com.Lugga.lugga.sharedpreferences.UsersharedPrefernce
import kotlinx.android.synthetic.main.activity_introduction.*

class IntroductionActivity : AppCompatActivity() {

    private var usersharedPrefernce: UsersharedPrefernce? = null
    private val fragmentList = ArrayList<Fragment>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val window = window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimary)


        setContentView(R.layout.activity_introduction)
        usersharedPrefernce = UsersharedPrefernce.getInstance()

        val adapter = IntroSliderAdapter(this)

        vpIntroSlider.adapter = adapter
        fragmentList.addAll(
            listOf(
                IntroFirstFragment(), IntroSecondFragment()
            )
        )
        adapter.setFragmentList(fragmentList)
        indicatorLayout.setIndicatorCount(adapter.itemCount)
        indicatorLayout.selectCurrentPosition(0)
        registerListeners()

    }

    private fun registerListeners() {
        vpIntroSlider.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                indicatorLayout.selectCurrentPosition(position)
                if (position < fragmentList.lastIndex) {
                    tvSkip.visibility = View.VISIBLE
                    tvNext.text = this@IntroductionActivity.getString(R.string.next)
                } else {
                    tvSkip.visibility = View.GONE
                    tvNext.text = this@IntroductionActivity.getString(R.string.getstarted)

                }
            }
        })
        tvSkip.setOnClickListener {
            if (usersharedPrefernce!!.getboolean()) {
                val intent = Intent(this, HomeActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
        tvNext.setOnClickListener {
            val position = vpIntroSlider.currentItem
            if (position < fragmentList.lastIndex) {
                vpIntroSlider.currentItem = position + 1
            } else {
                if (usersharedPrefernce!!.getboolean()) {
                    val intent = Intent(this, HomeActivity::class.java)
                    startActivity(intent)
                    finish()
                } else {
                    val intent = Intent(this, LoginActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }
        }
    }
}