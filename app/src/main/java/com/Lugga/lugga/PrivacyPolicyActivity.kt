package com.Lugga.lugga

import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity

class PrivacyPolicyActivity : AppCompatActivity(), View.OnClickListener {
    var webView: WebView? = null
    var ivBack: ImageView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_privacy_policy)
        webView = findViewById(R.id.webView)
        ivBack = findViewById(R.id.ivBack)
        webView!!.getSettings().loadsImagesAutomatically = true
        webView!!.getSettings().javaScriptEnabled = true
        webView!!.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY)
        webView!!.loadUrl("https://luggalanguages.com/privacy-policy")
        ivBack!!.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        onBackPressed()
    }
}